<?php
header('Content-Type: application/json');
require_once 'includes/config.php';
require_once('includes/application_top.php');
require_once('includes/classes/team.php');
@include_once('kint/Kint.class.php');

$week = (int)$_GET['week'];
if (empty($week)) {
    //get current week
    $week = (int)getCurrentWeek();
}

$cutoffDateTime = getCutoffDateTime($week);
$weekExpired = ((date("U", time() + (SERVER_TIMEZONE_OFFSET * 3600)) > strtotime($cutoffDateTime)) ? 1 : 0);
//get array of games
$allScoresIn = true;
$games = array();
//$sql = "select * from " . DB_PREFIX . "schedule where weekNum = " . $week . " order by gameTimeEastern, gameID";
//$query = $mysqli->query($sql);
//while ($row = $query->fetch_assoc()) {
//    $games[$row['gameID']]['gameID'] = $row['gameID'];
//    $games[$row['gameID']]['homeID'] = $row['homeID'];
//    $games[$row['gameID']]['visitorID'] = $row['visitorID'];
//    if (strlen($row['homeScore']) > 0 && strlen($row['visitorScore']) > 0) {
//        if ((int)$row['homeScore'] > (int)$row['visitorScore']) {
//            $games[$row['gameID']]['winnerID'] = $row['homeID'];
//        }
//        if ((int)$row['visitorScore'] > (int)$row['homeScore']) {
//            $games[$row['gameID']]['winnerID'] = $row['visitorID'];
//        }
//    } else {
//        $games[$row['gameID']]['winnerID'] = '';
//        $allScoresIn = false;
//    }
//}
//$query->free;

//get array of player picks
$playerPicks = array();
$playerTotals = array();
$sql_2 = "select p.userID, p.gameID, p.pickID, p.points, ps.tieBreakerPoints as tiePoints ";
$sql_2 .= "from " . DB_PREFIX . "picks p ";
$sql_2 .= "inner join " . DB_PREFIX . "users u on p.userID = u.userID ";
$sql_2 .= "inner join " . DB_PREFIX . "schedule s on p.gameID = s.gameID ";
$sql_2 .= "left outer join " . DB_PREFIX . "picksummary ps on u.userID = ps.userID and ps.weekNum = ".$week." ";
$sql_2 .= "where s.weekNum = " . $week . " and u.userName <> 'admin' ";
$sql_2 .= "order by p.userID, s.gameTimeEastern, s.gameID";
$query = $mysqli->query($sql_2) or die('Error inserting picks: ' . $mysqli->error);
$i = 0;
while ($row = $query->fetch_assoc()) {
    //var_dump($row['points']);
    // $playerPicks[$row['userID']][$row['gameID']] = $row['pickID'];
    // $playerPicks[$row['userID']]['points'][$row['gameID']] = $row['points'];
    // if (!is_null($row['tiePoints'])) $playerPicks[$row['userID']]['tiePoints'] = $row['tiePoints'];
    // if (!empty($games[$row['gameID']]['winnerID']) && $row['pickID'] == $games[$row['gameID']]['winnerID']) {
    //     //player has picked the winning team
    //     $playerTotals[$row['userID']] += $row['points'];
    // } else {
    //     $playerTotals[$row['userID']] += 0;
    // }
    // $i++;
    $games[] = $row;
    //d($row);
}
$query->free;
echo json_encode($games);
//var_dump($playerPicks);
//if (!$allScoresIn) {
//    echo '<p style="font-weight: bold; color: #DBA400;">* Not all scores have been updated for week ' . $week . ' yet.</p>' . "\n";
//}
//echo $login->get_user_by_id($userID);
//if (sizeof($playerTotals) > 0) {
//            $i = 0;
//            arsort($playerTotals);
//            foreach ($playerTotals as $userID => $totalCorrect) {
//                $hidePicks = hidePicks($userID, $week);
//                if ($i == 0) {
//                    $topScore = $totalCorrect;
//                    $winners[] = $userID;
//                } else if ($totalCorrect == $topScore) {
//                    $winners[] = $userID;
//                }
//                $tmpUser = $login->get_user_by_id($userID);
//                switch (USER_NAMES_DISPLAY) {
//                    case 1:
//                        echo '		<td>' . trim($tmpUser->firstname . ' ' . $tmpUser->lastname) . '</td>' . "\n";
//                        break;
//                    case 2:
//                        echo '		<td>' . trim($tmpUser->userName) . '</td>' . "\n";
//                        break;
//                    default: //3
//                        echo '		<td><abbr title="' . trim($tmpUser->firstname . ' ' . $tmpUser->lastname) . '">' . trim($tmpUser->userName) . '</abbr></td>' . "\n";
//                        break;
//                }
                //loop through all games
                //var_dump($playerPicks);
//                foreach ($games as $game) {
//                    $pick = '';
//                    $pick = $playerPicks[$userID][$game['gameID']];
//
//                    //$pts_per_game = $playerPicks[$userID][$game['points']];
//                    if (!empty($game['winnerID'])) {
//                        //score has been entered
//                        if ($playerPicks[$userID][$game['gameID']] == $game['winnerID']) {
//                            $pick = '<span class="winner">' . $pick . '</span>';
//                        } else if (($playerPicks[$userID][$game['gameID']] != $game['winnerID'])) $pick = '<span class="loser">' . $pick . '</span>';
//                    }
//                    elseif ($userID == $user->userID || $weekExpired || $user->userName == 'admin'){
//                        $pick = $playerPicks[$userID][$game['gameID']];
//                    }
//                    else {
//                        $pick = '***';
//
//                    }
//                    echo '		<td class="pickTD">' . $pick . '</td>' . "\n";
//
//
//                }
//                $tiePoints = isset($playerPicks[$userID]['tiePoints']) ? $playerPicks[$userID]['tiePoints'] : "Not Entered";
//
//                //echo '		<td nowrap><b>' . $totalCorrect . '/' . sizeof($games) . ' (' . number_format(($totalCorrect / sizeof($games)) * 100, 2) . '%)</b></td>' . "\n";
//
//                $i++;
//            }
    //display list of absent players
//    $sql = "select * from " . DB_PREFIX . "users where `status` = 1 and userID not in(" . implode(',', array_keys($playerTotals)) . ") and userName <> 'admin'";
//    $query = $mysqli->query($sql);
//    if ($query->num_rows > 0) {
//        $absentHtml = '<p><b>Absent Players:</b> ';
//        $i = 0;
//        while ($row = $query->fetch_assoc()) {
//            if ($i > 0) $absentHtml .= ', ';
//            switch (USER_NAMES_DISPLAY) {
//                case 1:
//                    $absentHtml .= trim($row['firstname'] . ' ' . $row['lastname']);
//                    break;
//                case 2:
//                    $absentHtml .= $row['userName'];
//                    break;
//                default: //3
//                    $absentHtml .= '<abbr title="' . trim($row['firstname'] . ' ' . $row['lastname']) . '">' . $row['userName'] . '</abbr>';
//                    break;
//            }
//            $i++;
//        }
//        echo $absentHtml;
//    }
//    $query->free;
//}

