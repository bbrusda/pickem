/**
 * Created by bdbtzc on 9/2/2015.
 */
$(document).ready(function () {
    $("#form").submit(function (e) {
        /*            $.ajax({
        
                        url: "dateRecd.php",
                        type: "POST",
                        data: $(this).serialize(),
        
                        success: function(data){
                            alert(data);
                            //  chatWith('9','name');
                        }
        
        
                    });*/

    });

    $('#myModal').on('shown.bs.modal', function () {
        $('#modal-pts').focus();
    })
});
$('.final-game').on('click', function () {
    //$(".final-game").removeClass('final-game');
    $('.hide-div').show();
    $('#myModal').modal('show');
});
$('#pickforme').on('click', function () {
    //$(".final-game").removeClass('final-game');
    $.ajax
        ({
            url: 'entry_form.php',
            data: {"pickforme": true, "week": $("#currweek").text()},
            type: 'post',
            success: function (result) {
                location.reload();
                //console.log(result);
            }
        });
})
$('#syncpicks').on('click', function () {
    //$(".final-game").removeClass('final-game');
    $.ajax
        ({
            url: 'entry_form.php',
            data: {"syncpicks": true, "week": $("#currweek").text()},
            type: 'post',
            success: function (result) {
                //location.reload();
                console.log(result);
            }
        });
})
$('#modal-pts').keypress(function (e) {
    var key = e.which;
    if (key == 13)  // the enter key code
    {
        $('.submit-modal').click();
        return false;
    }
});

$('#noPointsModal').keypress(function (e) {
    var key = e.which;
    if (key == 13)  // the enter key code
    {
        $('.close').click();
        return false;
    }
});

$('.submit-modal').on('click', function () {
    var tiePoints = $('#modal-pts').val();
    var gameID = $("#modal-pts").attr('data-gameID');
    if (tiePoints == "") {
        $('#noPointsModal').modal('toggle');
    }
    else {
        $.ajax
            ({
                url: 'entry_form.php',
                data: { "gameID": gameID, "ajax_tie": true, "tiePoints": tiePoints },
                type: 'post',
                success: function (result) {
                    $('#myModal').modal('toggle');
                    $(".show-class").show();
                    $("#tie").val(tiePoints);
                }
            });
    }
});

$('.submit-ajax').on('click', function () {

    console.dir($('.submit-ajax[data-gameid="188"]'));
    var gameID = $(this).attr('data-gameID');
    var teamID = $(this).attr('data-teamID');
    var loserID = $(this).attr('data-loserID');
    var pts = null;
    $("#sortable").find('li').each(function () {
        if ($(this).attr('data-gameid') == gameID) {
            pts = $(this).attr('data-pts');
            return false;
        }
    });
    $.ajax
        ({
            url: 'entry_form.php',
            data: { "gameID": gameID, "teamID": teamID, "ajax": true, "points": pts },
            type: 'post',
            success: function (result) {
                //console.log("Pick added: " + gameID + " :" + result +":");
                if(result.includes("Error")){
                   
                    $('#' + gameID + '-show-fail').fadeIn('fast').delay(750).fadeOut('slow');
                    console.log("error");
                }
                else{
                    $("#game" + gameID).removeClass('ui-state-disabled');
                    $("#game" + gameID).addClass('style_li_border');
                    setTimeout(function () {
                        $("#game" + gameID).removeClass('style_li_border');
                    }, 1250);

                    $("#sortable").disableSelection();


                    $('#' + gameID + '-show').fadeIn('fast').delay(750).fadeOut('slow');
                    $('#'+gameID).text(pts);
                    var strip_teamID = teamID.replace(/'/g, "");
                    var strip_loserID = loserID.replace(/'/g, "");
                    $("#l" + strip_teamID).removeClass('loser-color');
                    $("#l" + strip_teamID).addClass('winner-color');
                    $("#w" + strip_loserID).removeClass('winner-color');
                    $("#w" + strip_loserID).addClass('loser-color');
                    $("#w" + strip_loserID).attr("id", "l" + strip_loserID);
                    $("#l" + strip_teamID).attr("id", "w" + strip_teamID);
                }
            },
            error: function (result){
                console.log("no internet connection");
            }
        });
})

$('#tie').on('input', function () {
    var gameID = $(this).attr('data-tieID');
    var tiePoints = $('#tie').val();
    $.ajax
        ({
            url: 'entry_form.php',
            data: { "gameID": gameID, "ajax_tie": true, "tiePoints": tiePoints },
            type: 'post',
            success: function (result) {
                $(".show-class").show();
                $("#tiePts").html(tiePoints);
                console.log(result);
            }
        });
});
