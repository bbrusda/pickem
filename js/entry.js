/**
 * Created by bbrusda on 05/20/2017.
 */
var values_arr = new Array();
var k = 0;
var expired_games = new Array();
var entry_games = new Array();
var class_name = "";
var logo_class = "logo-desktop";

$(function(){
    $("body").on("shown.bs.modal", ".modal:not()", function () {
        $(this).css({
            'top': '45%',
            'margin-top': function () {
                return -($(this).height() / 2);
            }
        });
    });
    if(isMobileDevice()){
        class_name = "hide-details";
        logo_class = "logo-mobile";
        $(".logo").removeClass(".logo-desktop").addClass(".logo-mobile");
    }
    $("#loader").show();
    $.get("getgames.php?week="+getParameterByName('week'), function (data) {
        $.each(data, function (index, data_row) {
            if(data_row.expired === "1" || data_row.expired === 1){
                expired_games.push(data_row);
            }
            else{
                entry_games.push(data_row);
            }
            
        });
        setExpiredTable(expired_games);
        setEntryTable(entry_games);
        if(entry_games.length === 0){
            $("#save").hide();
            $("#entry-table").hide();
            $("#tie_breaker").val(expired_games[0].tie_points);
        }
        else{
            $("#tie_breaker").val(entry_games[0].tie_points);
        }

        $.each(entry_games, function(index, row){
            //set initial key value pair
            values_arr[index] = row.points;
        });
        $("#loader").hide();
    });
    $("#entry-table").on('reorder-row.bs.table', function (e, data) {
        setTimeout(function () {
            $.each(data, function (index, row) {
                //console.log(values_arr[index]);
                row.points = values_arr[index];
                $("#entry-table").bootstrapTable('updateRow', {
                    index: index,
                    row: row
                });

            })
        }, 0);
    });
    $("#save").click(function () {
        var valid = true;
        $.each($("#entry-table").bootstrapTable("getData"), function(index, row){
            console.dir(row);
            if(row.selected_visitor === 0 && row.selected_home === 0){
                valid = false;
            }
        });
        if(valid){
            //do normal procedure
            saveTable();
        }
        else{
            bootbox.confirm({
                message: "You didn't pick all the games.. Would you like to continue saving?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if(result){
                        saveTable();
                    }
                }
            });
        }
        console.log( $("#entry-table").bootstrapTable("getData").length);
    });
});
function saveTable(){
    if($("#tie_breaker").val() === ""){
        bootbox.alert("You forgot to enter a tie breaker...", function(){ $("#tie_breaker").addClass("error"); });
    }
    else{
        $("#small-loader").show();
        $("#save").prop("disabled", true);
        $.ajax
        ({
            url: 'saverecords.php',
            data: {data: $("#entry-table").bootstrapTable("getData"), week: getParameterByName('week'), tie_breaker: $("#tie_breaker").val()},
            type: 'post',
            success: function (result) {
                if(result === 'success'){
                    bootbox.alert("Your picks have been saved!", function(){ location.reload(); });
                    $("#small-loader").hide();
                }
                else{
                    bootbox.alert("Something went wrong.. Please contact bdbtzc@gmail.com", function(){ /*location.reload();*/ });
                }
            },
            error: function (res){
                bootbox.alert("Something went wrong.. Please contact bdbtzc@gmail.com", function(){ /*location.reload();*/ });
            }
        });
    }
}
window.operateEvents = {
    'click .select-box-home': function (e, value, row, index) {
        // console.dir(index);
        row.selected_home = 1;
        row.selected_visitor = 0;
        $("#entry-table").bootstrapTable('updateRow', {
            index: index,
            row: row
        });
    },
    'click .select-box-visitor': function (e, value, row, index) {
        row.selected_home = 0;
        row.selected_visitor = 1;
        $("#entry-table").bootstrapTable('updateRow', {
            index: index,
            row: row
        });
    },
    'click .details': function (e, value, row, index) {
        console.dir(row);
        var dialog = bootbox.dialog({
            title: 'Game Details',
            message: "<h4>"+row.formatted_game_time+"</h4><p>"+row.visitor+" ("+row.visitor_record+")</br> @ </br>"+row.home+" ("+row.home_record+")</p>",
            buttons: {
                ok: {
                    label: "Close",
                    className: 'btn-primary',
                    callback: function(){
                        //Example.show('Custom OK clicked');
                    }
                }
            }
            });
    }
}
function setEntryTable(data){
    $("#entry-table").bootstrapTable({
        data: data,
        columns: [{
            field: "gameID",
            title: "ID",
            visible: false
        }, {
                field: "points",
                title: "Points"
            },
            {
                field: "formatted_game_time",
                title: "Game Time",
                class: class_name
                //formatter: operateFormatterGameDetails
            }, {
                field: 'visitorID',
                class: 'select-team',
                title: "Away",
                formatter: operateFormatterSelectedVisitor,
                cellStyle: cellStyleVisitor,
                events: operateEvents
            },{
                field: "at",
                visible: true
            }, {
                field: 'homeID',
                class: 'select-team',
                title: "Home",
                formatter: operateFormatterSelectedHome,
                cellStyle: cellStyleHome,
                events: operateEvents
            },{
                field: "",
                title: "Details",
                //class: class_name,
                formatter: operateFormatterMoreDetails,
                events: operateEvents
            }]
    });
}
function operateFormatterMoreDetails(value, row, index) {
    return '<button type="button" class="btn btn-primary btn-xs details">More</button>';
}
function isMobileDevice() {
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};
function setExpiredTable(data){
    $("#expired-table").bootstrapTable({
        data: data,
        columns: [{
            field: "gameID",
            title: "ID",
            visible: false
        }, {
                field: "points",
                title: "Points"
            },{
                field: "formatted_game_time",
                title: "Game Time",
                class: class_name
                //formatter: operateFormatterGameDetails
            }, {
                field: 'visitorID',
                class: 'select-team',
                title: "Away Team",
                formatter: operateFormatterExpiredVisitor,
            }, {
                field: 'homeID',
                class: 'select-team',
                title: "Home Team",
                formatter: operateFormatterExpiredHome,
            }]
    });
}
function formatExpired(row, is_selected, logo, type){
    var glyph = "";
    if(row.pickID === null || row.pickID === undefined || row.pickID === ""){
        //forgot to pick game
        glyph = "unchecked";
    }
    else if(is_selected === 1){
        //selected visitor
        glyph = row.pickedCorrect ? "check" : "remove";
    }
    else{
        glyph = "unchecked";
    }
    var pts = type === "home" ? row.homeScore : row.visitorScore;
    return [
        '<p class="select-box-home" href="javascript:void(0)" >',
        '<i class="glyphicon glyphicon-'+glyph+'"></i><img class="'+logo_class+'" src="images/logos/' + logo + '.svg"/>',
        pts,
        '</p>'
    ].join('');
}
function operateFormatterExpiredVisitor(value, row, index) {
    return formatExpired(row, row.selected_visitor, row.visitorID, "visitor");
    //game is expired 
}
function operateFormatterExpiredHome(value, row, index) {
    return formatExpired(row, row.selected_home, row.homeID, "home"); 
}
function operateFormatterGameDetails(value, row, index) {
    return [
        '<span>',
        '' + row.visitor + ' @ ' + row.home + ' - ' + row.formatted_game_time,
        '</span>'
    ].join('');
}
function cellStyleHome(value, row, index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];
    if (row.selected_home === 1) {
        return {
            classes: classes[1]
        };
    }
    else {
        return [];
    }
}
function cellStyleVisitor(value, row, index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];
    if (row.selected_visitor === 1) {
        return {
            classes: classes[1]
        };
    }
    else {
        return [];
    }
}
function operateFormatterSelectedHome(value, row, index) {
    if(row.change && value === "KC"){
        value = "KCS";
    }
    var glyph = "";
    if (row.selected_home === 1) {
        glyph = "check";
    }
    else {
        glyph = "unchecked";
    }
    return [
            '<a class="select-box-home" href="javascript:void(0)" >',
            '<i class="glyphicon glyphicon-'+glyph+'"></i><img class="'+logo_class+'" src="images/logos/' + value + '.svg"/>',
            '<span class="'+class_name+'">' +row.home+' ('+row.home_record+')</span></a>'
        ].join('');
}
function operateFormatterSelectedVisitor(value, row, index) {
    if(row.change && value === "KC"){
        value = "KCS";
    }
    var glyph = "";
    if (row.selected_visitor === 1) {
       glyph = "check";
    }
    else {
        glyph = "unchecked";
    }
     return [
            '<a class="select-box-visitor" href="javascript:void(0)">',
            '<i class="glyphicon glyphicon-'+glyph+'"></i><img class="'+logo_class+'" src="images/logos/' + value + '.svg"/>',
            '<span class="'+class_name+'">' +row.visitor+' ('+row.visitor_record+')</span></a>'
        ].join('');
}
function expiredRow(row, index){
    
    if(row.pickedCorrect === true || row.pickedCorrect === "true"){
        return {classes: "success"}
    }
    else if(row.pickedCorrect === false || row.pickedCorrect === "false"){
        return {classes: "danger"}
    }
    else{
        return {classes: ""}   
    }
}
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
