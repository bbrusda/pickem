/**
 * Created by bdbtzc on 9/1/2015.
 */

$(function() {
    var locked_games = Array();
    $('#unsortable').find('li').each(function () {
        if ($(this).hasClass("ui-state-disabled-game")) {
            var gameID = $(this).attr('data-gameID');
            var pt_val = parseInt($(this).attr('data-points'));
            locked_games.push(pt_val);
        }
    });
    $( "#sortable" ).sortable({
        placeholder: "ui-state-highlight",
        update: function(event, ui){
            var items = Array();
            ui.item.parent().find('li').each(function(){
                var gameID = $(this).attr('data-gameID');
                var teamID = $(this).attr('data-teamID');
                //var pts = $(this).attr('data-pts');
                var pts = $(this).index() + 1;
                locked_games.forEach(function(currentValue){
                    if(currentValue == pts) {
                        pts++;
                    }
                });

                while(pts <= $(this).prev().find('.pts').html()){
                    pts++;
                    locked_games.forEach(function(currentValue){
                        if(currentValue == pts) {
                            pts++;
                        }
                    });
                }
                if(!($(this).hasClass('ui-state-disabled'))){
                    $('#'+gameID).text(pts);
                }
                //console.dir($(this));
                $('#game'+gameID).attr('data-pts', pts); 
                $(this).find('.pts').html(pts);
                if(!($(this).hasClass("ui-state-disabled"))) {
                    var obj = {"gameID": gameID, "teamID": teamID, "points": pts};
                    items.push(obj);
                }
            });

            $.ajax
            ({
                url: 'entry_form.php',
                data: {items: items, "ajax_pts": true},
                type: 'post',
                success: function(result)
                {
                    //console.log(result);
                }
            });
        }
    });

    var isMobile = false; //initiate as false
// device detection
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
    if(isMobile == false) {
        //console.log("test");
        $(window).scroll(sticky_relocate);
        sticky_relocate();
    }
    else {
        //$("#sortable li").removeClass("style_li");
        //$("#sortable li").addClass("style_li_mobile");
        $(".alert-success-update").css('margin-left', '-25px');
    }
    $( "#sortable" ).sortable({
        cancel: ".ui-state-disabled",
        items: "li:not(.ui-state-disabled-item)"
    });
    $( "#sortable li" ).disableSelection();
    //$("#sortable").sortable('disable');
});

function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top;
    if (window_top > div_top) {
        $('#sticky').addClass('stick');
    } else {
        $('#sticky').removeClass('stick');
    }
}