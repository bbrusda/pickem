<?php
/**
 * Created by PhpStorm.
 * User: bbrusda
 * Date: 05/20/2017
 * Time: 3:11 PM
 */
header('Content-Type: application/json');
require_once 'includes/config.php';
require_once('includes/application_top.php');
require_once('includes/classes/team.php');
@include_once('kint/Kint.class.php');

$week = (int)$_GET['week'];
if (empty($week)) {
    //get current week
    $week = (int)getCurrentWeek();
}
$sql_1 = "select s.*,p.pickID, p.points, CONCAT(vt.city, ' ', vt.team) as visitor, CONCAT(ht.city, ' ', ht.team) as home, ";
$sql_1 .= "(DATE_ADD(NOW(), INTERVAL " . SERVER_TIMEZONE_OFFSET . " HOUR) > gameTimeEastern)  as expired, ";
$sql_1 .= "(SELECT tieBreakerPoints FROM " . DB_PREFIX . "picksummary WHERE userID = '".$user->userID."' and weekNum =" . $week . ") as tiePoints ";
$sql_1 .= "from " . DB_PREFIX . "schedule s ";
$sql_1 .= "inner join " . DB_PREFIX . "teams ht on s.homeID = ht.teamID ";
$sql_1 .= "inner join " . DB_PREFIX . "teams vt on s.visitorID = vt.teamID ";
$sql_1 .= "left outer join nflp_picks p on (p.gameID = s.gameID and p.userID = '".$user->userID."') ";
$sql_1 .= "where s.weekNum = " . $week . " ";
$sql_1 .= "order by s.gameTimeEastern, s.gameID";
//echo $sql_1;
$query = $mysqli->query($sql_1) or die($mysqli->error);
$i = 1;
$games = array();
$all_games = array();
$not_used = array();
$used_numbers = array();
$num_games = $query->num_rows;
if ($num_games > 0) {
    while ($row = $query->fetch_assoc()) {
        $row["formatted_game_time"] = date('D n/j g:i a', strtotime($row['gameTimeEastern'])) . " ET";
        $row["home_record"] = getTeamRecord($row["homeID"]);
        $row["visitor_record"] =getTeamRecord($row["visitorID"]);
        $row["tie_points"] = $row["tiePoints"];
        $row["pickID"] == $row["homeID"] ? $row["selected_home"] = 1 : $row['selected_home'] = 0;
        $row["pickID"] == $row["visitorID"] ? $row["selected_visitor"] = 1 : $row['selected_visitor'] = 0;
        // if(is_null($row["visitorScore"]) && is_null($row["homeScore"])){  
        //     //scores have not been entered
        //     $row["pickedCorrect"] = "N/A";
        // }
        if($user->userID == 5 || $user->userID == 10){
            $row["change"] = true;
        }
        else{
            $row["change"] = false;
        }

        if($row["expired"] && is_null($row["pickID"])){
            $row["forgot"] = true;
        }
        else{
            $row["forgot"] = false;
        }
        if($row["visitorScore"] > $row["homeScore"] && $row["pickID"] == $row["visitorID"]){
            $row["pickedCorrect"] = true;
        }
        elseif($row["homeScore"] > $row["visitorScore"] && $row["pickID"] == $row["homeID"]){
            $row["pickedCorrect"] = true;
        }
        elseif($row["expired"] && (is_null($row["homeScore"]) && is_null($row["visitorScore"]))){
            $row["pickedCorrect"] = "noscores";
        }
        else{
            $row["pickedCorrect"] = false;
        }
        
        //$row['points'] = (int)$row["points"];
        if(!is_null($row['points'])){
            //game has been picked
            $row['points'] = (int)$row["points"];
            $used_numbers[] = $row["points"];
        }
        $games[] = $row;
    }
    $i = $num_games;
    while($i >= 1){
        //build array of not used numbers
        if(!in_array($i, $used_numbers)){
            $not_used[] = $i;
        }
        $i--;
    }
    
    $i = 0;
    foreach ($games as $game){
        if(is_null($game["points"])){
            //game has not been picked, assign it a not used number
           $game["points"] = (int)$not_used[$i++];
        }  
        $game['at'] = "@";
        if($game["forgot"]){
            //echo "INSERT";
            insertHighestAvailblePts($game["gameID"], $user->userID, $game["points"], $mysqli);
        }
        $all_games[] = $game;
    }
}

usort($all_games, "cmp");
echo json_encode($all_games);
function cmp($a, $b)
{
    //var_dump($a);
    return   $b['points'] - $a['points'];
}
function insertHighestAvailblePts($gameID, $userID, $pts, $mysqli){
    //echo "INSERT " . $gameID . " for user " . $userID . " for " . $pts; 
    $sql = "INSERT INTO " . DB_PREFIX . "picks (userID, gameID, pickID, points) VALUES ";
    $sql .= "('.$userID.', '.$gameID.', 'N/A', '.$pts.')";
    //echo $sql_1;
    $query = $mysqli->query($sql) or die($mysqli->error);
}