-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: nflpickem
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.21-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `nflp_comments`
--

DROP TABLE IF EXISTS `nflp_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nflp_comments` (
  `commentID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `comment` longtext NOT NULL,
  `postDateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`commentID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nflp_comments`
--

LOCK TABLES `nflp_comments` WRITE;
/*!40000 ALTER TABLE `nflp_comments` DISABLE KEYS */;
INSERT INTO `nflp_comments` VALUES (1,2,'Test123','Test','2015-08-26 16:12:37');
/*!40000 ALTER TABLE `nflp_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nflp_divisions`
--

DROP TABLE IF EXISTS `nflp_divisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nflp_divisions` (
  `divisionID` int(11) NOT NULL AUTO_INCREMENT,
  `conference` varchar(10) NOT NULL,
  `division` varchar(32) NOT NULL,
  PRIMARY KEY (`divisionID`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nflp_divisions`
--

LOCK TABLES `nflp_divisions` WRITE;
/*!40000 ALTER TABLE `nflp_divisions` DISABLE KEYS */;
INSERT INTO `nflp_divisions` VALUES (1,'AFC','North'),(2,'AFC','South'),(3,'AFC','East'),(4,'AFC','West'),(5,'NFC','North'),(6,'NFC','South'),(7,'NFC','East'),(8,'NFC','West');
/*!40000 ALTER TABLE `nflp_divisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nflp_email_templates`
--

DROP TABLE IF EXISTS `nflp_email_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nflp_email_templates` (
  `email_template_key` varchar(255) NOT NULL,
  `email_template_title` varchar(255) NOT NULL,
  `default_subject` varchar(255) DEFAULT NULL,
  `default_message` text,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`email_template_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nflp_email_templates`
--

LOCK TABLES `nflp_email_templates` WRITE;
/*!40000 ALTER TABLE `nflp_email_templates` DISABLE KEYS */;
INSERT INTO `nflp_email_templates` VALUES ('WEEKLY_PICKS_REMINDER','Weekly Picks Reminder','NFL Pick \'Em Week {week} Reminder','Hello {player},<br /><br />You are receiving this email because you do not yet have all of your picks in for week {week}.&nbsp; This is your reminder.&nbsp; The first game is {first_game} (Eastern), so to receive credit for that game, you\'ll have to make your pick before then.<br /><br />Links:<br />&nbsp;- NFL Pick \'Em URL: {site_url}<br />&nbsp;- Help/Rules: {rules_url}<br /><br />Good Luck!<br />','NFL Pick \'Em Week {week} Reminder','Hello {player},<br /><br />You are receiving this email because you do not yet have all of your picks in for week {week}.&nbsp; This is your reminder.&nbsp; The first game is {first_game} (Eastern), so to receive credit for that game, you\'ll have to make your pick before then.<br /><br />Links:<br />&nbsp;- NFL Pick \'Em URL: {site_url}<br />&nbsp;- Help/Rules: {rules_url}<br /><br />Good Luck!<br />'),('WEEKLY_RESULTS_REMINDER','Last Week Results/Reminder','NFL Pick \'Em Week {previousWeek} Standings/Reminder','Congratulations this week go to {winners} for winning week {previousWeek}.  The winner(s) had {winningScore} out of {possibleScore} picks correct.<br /><br />The current leaders are:<br />{currentLeaders}<br /><br />The most accurate players are:<br />{bestPickRatios}<br /><br />*Reminder* - Please make your picks for week {week} before {first_game} (Eastern).<br /><br />Links:<br />&nbsp;- NFL Pick \'Em URL: {site_url}<br />&nbsp;- Help/Rules: {rules_url}<br /><br />Good Luck!<br />','NFL Pick \'Em Week {previousWeek} Standings/Reminder','Congratulations this week go to {winners} for winning week {previousWeek}.  The winner(s) had {winningScore} out of {possibleScore} picks correct.<br /><br />The current leaders are:<br />{currentLeaders}<br /><br />The most accurate players are:<br />{bestPickRatios}<br /><br />*Reminder* - Please make your picks for week {week} before {first_game} (Eastern).<br /><br />Links:<br />&nbsp;- NFL Pick \'Em URL: {site_url}<br />&nbsp;- Help/Rules: {rules_url}<br /><br />Good Luck!<br />'),('FINAL_RESULTS','Final Results','NFL Pick \'Em 2015 Final Results','Congratulations this week go to {winners} for winning week\r\n{previousWeek}. The winner(s) had {winningScore} out of {possibleScore}\r\npicks correct.<br /><br /><span style=\"font-weight: bold;\">Congratulations to {final_winner}</span> for winning NFL Pick \'Em 2015!&nbsp; {final_winner} had {final_winningScore} wins and had a pick ratio of {picks}/{possible} ({pickpercent}%).<br /><br />Top Wins:<br />{currentLeaders}<br /><br />The most accurate players are:<br />{bestPickRatios}<br /><br />Thanks for playing, and I hope to see you all again for NFL Pick \'Em 2012!','NFL Pick \'Em 2015 Final Results','Congratulations this week go to {winners} for winning week\r\n{previousWeek}. The winner(s) had {winningScore} out of {possibleScore}\r\npicks correct.<br /><br /><span style=\"font-weight: bold;\">Congratulations to {final_winner}</span> for winning NFL Pick \'Em 2015!&nbsp; {final_winner} had {final_winningScore} wins and had a pick ratio of {picks}/{possible} ({pickpercent}%).<br /><br />Top Wins:<br />{currentLeaders}<br /><br />The most accurate players are:<br />{bestPickRatios}<br /><br />Thanks for playing, and I hope to see you all again for NFL Pick \'Em 2012!');
/*!40000 ALTER TABLE `nflp_email_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nflp_picks`
--

DROP TABLE IF EXISTS `nflp_picks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nflp_picks` (
  `userID` int(11) NOT NULL,
  `gameID` int(11) NOT NULL,
  `pickID` varchar(10) NOT NULL,
  `points` int(11) DEFAULT NULL,
  PRIMARY KEY (`userID`,`gameID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nflp_picks`
--

LOCK TABLES `nflp_picks` WRITE;
/*!40000 ALTER TABLE `nflp_picks` DISABLE KEYS */;
INSERT INTO `nflp_picks` VALUES (5,1,'CAR',16),(5,2,'ATL',12),(5,3,'BAL',1),(5,4,'CHI',2),(5,5,'GB',4),(5,6,'SD',5),(5,7,'NO',6),(5,8,'NYJ',10),(5,9,'CLE',7),(5,10,'MIN',9),(5,11,'MIA',3),(5,12,'DAL',11),(5,13,'IND',13),(5,14,'NE',8),(5,15,'WAS',14),(5,16,'SF',15);
/*!40000 ALTER TABLE `nflp_picks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nflp_picksummary`
--

DROP TABLE IF EXISTS `nflp_picksummary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nflp_picksummary` (
  `weekNum` int(11) NOT NULL DEFAULT '0',
  `userID` int(11) NOT NULL DEFAULT '0',
  `tieBreakerPoints` int(11) NOT NULL,
  `showPicks` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`weekNum`,`userID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nflp_picksummary`
--

LOCK TABLES `nflp_picksummary` WRITE;
/*!40000 ALTER TABLE `nflp_picksummary` DISABLE KEYS */;
/*!40000 ALTER TABLE `nflp_picksummary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nflp_schedule`
--

DROP TABLE IF EXISTS `nflp_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nflp_schedule` (
  `gameID` int(11) NOT NULL AUTO_INCREMENT,
  `weekNum` int(11) NOT NULL,
  `gameTimeEastern` datetime DEFAULT NULL,
  `homeID` varchar(10) NOT NULL,
  `homeScore` int(11) DEFAULT NULL,
  `visitorID` varchar(10) NOT NULL,
  `visitorScore` int(11) DEFAULT NULL,
  `overtime` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`gameID`),
  KEY `GameID` (`gameID`),
  KEY `HomeID` (`homeID`),
  KEY `VisitorID` (`visitorID`)
) ENGINE=MyISAM AUTO_INCREMENT=257 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nflp_schedule`
--

LOCK TABLES `nflp_schedule` WRITE;
/*!40000 ALTER TABLE `nflp_schedule` DISABLE KEYS */;
INSERT INTO `nflp_schedule` VALUES (1,1,'2017-09-07 20:30:00','DEN',0,'CAR',0,0),(2,1,'2017-09-10 13:00:00','ATL',0,'TB',0,0),(3,1,'2017-09-10 13:00:00','BAL',0,'BUF',0,0),(4,1,'2017-09-10 13:00:00','HOU',0,'CHI',0,0),(5,1,'2017-09-10 13:00:00','JAX',0,'GB',0,0),(6,1,'2017-09-10 13:00:00','KC',0,'SD',0,0),(7,1,'2017-09-10 13:00:00','NO',0,'OAK',0,0),(8,1,'2017-09-10 13:00:00','NYJ',0,'CIN',0,0),(9,1,'2017-09-10 13:00:00','PHI',0,'CLE',0,0),(10,1,'2017-09-10 13:00:00','TEN',0,'MIN',0,0),(11,1,'2017-09-10 16:05:00','SEA',0,'MIA',0,0),(12,1,'2017-09-10 16:25:00','DAL',0,'NYG',0,0),(13,1,'2017-09-10 16:25:00','IND',0,'DET',0,0),(14,1,'2017-09-10 20:30:00','ARI',0,'NE',0,0),(15,1,'2017-09-11 19:10:00','WAS',0,'PIT',0,0),(16,1,'2017-09-11 22:20:00','SF',0,'LA',0,0),(17,2,'2017-09-14 20:25:00','BUF',0,'NYJ',0,0),(18,2,'2017-09-17 13:00:00','CAR',0,'SF',0,0),(19,2,'2017-09-17 13:00:00','CLE',0,'BAL',0,0),(20,2,'2017-09-17 13:00:00','DET',0,'TEN',0,0),(21,2,'2017-09-17 13:00:00','HOU',0,'KC',0,0),(22,2,'2017-09-17 13:00:00','NE',0,'MIA',0,0),(23,2,'2017-09-17 13:00:00','NYG',0,'NO',0,0),(24,2,'2017-09-17 13:00:00','PIT',0,'CIN',0,0),(25,2,'2017-09-17 13:00:00','WAS',0,'DAL',0,0),(26,2,'2017-09-17 16:05:00','ARI',0,'TB',0,0),(27,2,'2017-09-17 16:05:00','LA',0,'SEA',0,0),(28,2,'2017-09-17 16:25:00','DEN',0,'IND',0,0),(29,2,'2017-09-17 16:25:00','OAK',0,'ATL',0,0),(30,2,'2017-09-17 16:25:00','SD',0,'JAX',0,0),(31,2,'2017-09-17 20:30:00','MIN',0,'GB',0,0),(32,2,'2017-09-18 20:30:00','CHI',0,'PHI',0,0),(33,3,'2017-09-21 20:25:00','NE',0,'HOU',0,0),(34,3,'2017-09-24 13:00:00','BUF',0,'ARI',0,0),(35,3,'2017-09-24 13:00:00','CAR',0,'MIN',0,0),(36,3,'2017-09-24 13:00:00','CIN',0,'DEN',0,0),(37,3,'2017-09-24 13:00:00','GB',0,'DET',0,0),(38,3,'2017-09-24 13:00:00','JAX',0,'BAL',0,0),(39,3,'2017-09-24 13:00:00','MIA',0,'CLE',0,0),(40,3,'2017-09-24 13:00:00','NYG',0,'WAS',0,0),(41,3,'2017-09-24 13:00:00','TEN',0,'OAK',0,0),(42,3,'2017-09-24 16:05:00','SEA',0,'SF',0,0),(43,3,'2017-09-24 16:05:00','TB',0,'LA',0,0),(44,3,'2017-09-24 16:25:00','PHI',0,'PIT',0,0),(45,3,'2017-09-24 16:25:00','IND',0,'SD',0,0),(46,3,'2017-09-24 16:25:00','KC',0,'NYJ',0,0),(47,3,'2017-09-24 20:30:00','DAL',0,'CHI',0,0),(48,3,'2017-09-25 20:30:00','NO',0,'ATL',0,0),(49,4,'2017-09-28 20:25:00','CIN',0,'MIA',0,0),(50,4,'2017-10-01 21:30:00','JAX',0,'IND',0,0),(51,4,'2017-10-01 13:00:00','ATL',0,'CAR',0,0),(52,4,'2017-10-01 13:00:00','BAL',0,'OAK',0,0),(53,4,'2017-10-01 13:00:00','CHI',0,'DET',0,0),(54,4,'2017-10-01 13:00:00','HOU',0,'TEN',0,0),(55,4,'2017-10-01 13:00:00','NE',0,'BUF',0,0),(56,4,'2017-10-01 13:00:00','NYJ',0,'SEA',0,0),(57,4,'2017-10-01 13:00:00','WAS',0,'CLE',0,0),(58,4,'2017-10-01 16:05:00','TB',0,'DEN',0,0),(59,4,'2017-10-01 16:25:00','ARI',0,'LA',0,0),(60,4,'2017-10-01 16:25:00','SD',0,'NO',0,0),(61,4,'2017-10-01 16:25:00','SF',0,'DAL',0,0),(62,4,'2017-10-01 20:30:00','PIT',0,'KC',0,0),(63,4,'2017-10-02 20:30:00','MIN',0,'NYG',0,0),(64,5,'2017-10-05 20:25:00','SF',0,'ARI',0,0),(65,5,'2017-10-08 13:00:00','CLE',0,'NE',0,0),(66,5,'2017-10-08 13:00:00','DET',0,'PHI',0,0),(67,5,'2017-10-08 13:00:00','IND',0,'CHI',0,0),(68,5,'2017-10-08 13:00:00','MIA',0,'TEN',0,0),(69,5,'2017-10-08 13:00:00','BAL',0,'WAS',0,0),(70,5,'2017-10-08 13:00:00','MIN',0,'HOU',0,0),(71,5,'2017-10-08 13:00:00','PIT',0,'NYJ',0,0),(72,5,'2017-10-08 16:05:00','DEN',0,'ATL',0,0),(73,5,'2017-10-08 16:25:00','DAL',0,'CIN',0,0),(74,5,'2017-10-08 16:25:00','LA',0,'BUF',0,0),(75,5,'2017-10-08 16:25:00','OAK',0,'SD',0,0),(76,5,'2017-10-08 20:30:00','GB',0,'NYG',0,0),(77,5,'2017-10-09 20:30:00','CAR',0,'TB',0,0),(78,6,'2017-10-12 20:25:00','SD',0,'DEN',0,0),(79,6,'2017-10-15 13:00:00','BUF',0,'SF',0,0),(80,6,'2017-10-15 13:00:00','CHI',0,'JAX',0,0),(81,6,'2017-10-15 13:00:00','DET',0,'LA',0,0),(82,6,'2017-10-15 13:00:00','MIA',0,'PIT',0,0),(83,6,'2017-10-15 13:00:00','NE',0,'CIN',0,0),(84,6,'2017-10-15 13:00:00','NO',0,'CAR',0,0),(85,6,'2017-10-15 13:00:00','NYG',0,'BAL',0,0),(86,6,'2017-10-15 13:00:00','TEN',0,'CLE',0,0),(87,6,'2017-10-15 13:00:00','WAS',0,'PHI',0,0),(88,6,'2017-10-15 16:05:00','OAK',0,'KC',0,0),(89,6,'2017-10-15 16:25:00','GB',0,'DAL',0,0),(90,6,'2017-10-15 16:25:00','SEA',0,'ATL',0,0),(91,6,'2017-10-15 20:30:00','HOU',0,'IND',0,0),(92,6,'2017-10-16 20:30:00','ARI',0,'NYJ',0,0),(93,7,'2017-10-19 20:25:00','GB',0,'CHI',0,0),(94,7,'2017-10-22 21:30:00','LA',0,'NYG',0,0),(95,7,'2017-10-22 13:00:00','CIN',0,'CLE',0,0),(96,7,'2017-10-22 13:00:00','DET',0,'WAS',0,0),(97,7,'2017-10-22 13:00:00','JAX',0,'OAK',0,0),(98,7,'2017-10-22 13:00:00','KC',0,'NO',0,0),(99,7,'2017-10-22 13:00:00','MIA',0,'BUF',0,0),(100,7,'2017-10-22 13:00:00','NYJ',0,'BAL',0,0),(101,7,'2017-10-22 13:00:00','PHI',0,'MIN',0,0),(102,7,'2017-10-22 13:00:00','TEN',0,'IND',0,0),(103,7,'2017-10-22 16:05:00','ATL',0,'SD',0,0),(104,7,'2017-10-22 16:05:00','SF',0,'TB',0,0),(105,7,'2017-10-22 16:25:00','PIT',0,'NE',0,0),(106,7,'2017-10-22 20:30:00','ARI',0,'SEA',0,0),(107,7,'2017-10-23 20:30:00','DEN',0,'HOU',0,0),(108,8,'2017-10-26 20:25:00','TEN',0,'JAX',0,0),(109,8,'2017-10-29 21:30:00','CIN',0,'WAS',0,0),(110,8,'2017-10-29 13:00:00','ATL',0,'GB',0,0),(111,8,'2017-10-29 13:00:00','BUF',0,'NE',0,0),(112,8,'2017-10-29 13:00:00','CLE',0,'NYJ',0,0),(113,8,'2017-10-29 13:00:00','HOU',0,'DET',0,0),(114,8,'2017-10-29 13:00:00','IND',0,'KC',0,0),(115,8,'2017-10-29 13:00:00','NO',0,'SEA',0,0),(116,8,'2017-10-29 13:00:00','TB',0,'OAK',0,0),(117,8,'2017-10-29 16:05:00','DEN',0,'SD',0,0),(118,8,'2017-10-29 16:25:00','CAR',0,'ARI',0,0),(119,8,'2017-10-29 20:30:00','DAL',0,'PHI',0,0),(120,8,'2017-10-30 20:30:00','CHI',0,'MIN',0,0),(121,9,'2017-11-02 20:25:00','TB',0,'ATL',0,0),(122,9,'2017-11-05 13:00:00','BAL',0,'PIT',0,0),(123,9,'2017-11-05 13:00:00','CLE',0,'DAL',0,0),(124,9,'2017-11-05 13:00:00','KC',0,'JAX',0,0),(125,9,'2017-11-05 13:00:00','MIA',0,'NYJ',0,0),(126,9,'2017-11-05 13:00:00','MIN',0,'DET',0,0),(127,9,'2017-11-05 13:00:00','NYG',0,'PHI',0,0),(128,9,'2017-11-05 16:05:00','LA',0,'CAR',0,0),(129,9,'2017-11-05 16:05:00','SF',0,'NO',0,0),(130,9,'2017-11-05 16:25:00','GB',0,'IND',0,0),(131,9,'2017-11-05 16:25:00','SD',0,'TEN',0,0),(132,9,'2017-11-05 20:30:00','OAK',0,'DEN',0,0),(133,9,'2017-11-06 20:30:00','SEA',0,'BUF',0,0),(134,10,'2017-11-09 20:25:00','BAL',0,'CLE',0,0),(135,10,'2017-11-12 13:00:00','JAX',0,'HOU',0,0),(136,10,'2017-11-12 13:00:00','NO',0,'DEN',0,0),(137,10,'2017-11-12 13:00:00','NYJ',0,'LA',0,0),(138,10,'2017-11-12 13:00:00','PHI',0,'ATL',0,0),(139,10,'2017-11-12 13:00:00','CAR',0,'KC',0,0),(140,10,'2017-11-12 13:00:00','TB',0,'CHI',0,0),(141,10,'2017-11-12 13:00:00','TEN',0,'GB',0,0),(142,10,'2017-11-12 13:00:00','WAS',0,'MIN',0,0),(143,10,'2017-11-12 16:05:00','SD',0,'MIA',0,0),(144,10,'2017-11-12 16:25:00','ARI',0,'SF',0,0),(145,10,'2017-11-12 16:25:00','PIT',0,'DAL',0,0),(146,10,'2017-11-12 20:30:00','NE',0,'SEA',0,0),(147,10,'2017-11-13 20:30:00','NYG',0,'CIN',0,0),(148,11,'2017-11-16 20:25:00','CAR',0,'NO',0,0),(149,11,'2017-11-19 13:00:00','CLE',0,'PIT',0,0),(150,11,'2017-11-19 13:00:00','DAL',0,'BAL',0,0),(151,11,'2017-11-19 13:00:00','DET',0,'JAX',0,0),(152,11,'2017-11-19 13:00:00','IND',0,'TEN',0,0),(153,11,'2017-11-19 13:00:00','CIN',0,'BUF',0,0),(154,11,'2017-11-19 13:00:00','KC',0,'TB',0,0),(155,11,'2017-11-19 13:00:00','MIN',0,'ARI',0,0),(156,11,'2017-11-19 13:00:00','NYG',0,'CHI',0,0),(157,11,'2017-11-19 16:05:00','LA',0,'MIA',0,0),(158,11,'2017-11-19 16:25:00','SF',0,'NE',0,0),(159,11,'2017-11-19 16:25:00','SEA',0,'PHI',0,0),(160,11,'2017-11-19 20:30:00','WAS',0,'GB',0,0),(161,11,'2017-11-20 20:30:00','OAK',0,'HOU',0,0),(162,12,'2017-11-23 12:30:00','DET',0,'MIN',0,0),(163,12,'2017-11-23 16:30:00','DAL',0,'WAS',0,0),(164,12,'2017-11-23 20:30:00','IND',0,'PIT',0,0),(165,12,'2017-11-26 13:00:00','CHI',0,'TEN',0,0),(166,12,'2017-11-26 13:00:00','BUF',0,'JAX',0,0),(167,12,'2017-11-26 13:00:00','BAL',0,'CIN',0,0),(168,12,'2017-11-26 13:00:00','ATL',0,'ARI',0,0),(169,12,'2017-11-26 13:00:00','CLE',0,'NYG',0,0),(170,12,'2017-11-26 13:00:00','HOU',0,'SD',0,0),(171,12,'2017-11-26 13:00:00','MIA',0,'SF',0,0),(172,12,'2017-11-26 13:00:00','NO',0,'LA',0,0),(173,12,'2017-11-26 16:05:00','TB',0,'SEA',0,0),(174,12,'2017-11-26 16:25:00','DEN',0,'KC',0,0),(175,12,'2017-11-26 16:25:00','OAK',0,'CAR',0,0),(176,12,'2017-11-26 20:30:00','NYJ',0,'NE',0,0),(177,12,'2017-11-27 20:30:00','PHI',0,'GB',0,0),(178,13,'2017-11-30 20:25:00','MIN',0,'DAL',0,0),(179,13,'2017-12-03 13:00:00','ATL',0,'KC',0,0),(180,13,'2017-12-03 13:00:00','BAL',0,'MIA',0,0),(181,13,'2017-12-03 13:00:00','CHI',0,'SF',0,0),(182,13,'2017-12-03 13:00:00','CIN',0,'PHI',0,0),(183,13,'2017-12-03 13:00:00','GB',0,'HOU',0,0),(184,13,'2017-12-03 13:00:00','JAX',0,'DEN',0,0),(185,13,'2017-12-03 13:00:00','NE',0,'LA',0,0),(186,13,'2017-12-03 13:00:00','NO',0,'DET',0,0),(187,13,'2017-12-03 16:05:00','OAK',0,'BUF',0,0),(188,13,'2017-12-03 16:25:00','ARI',0,'WAS',0,0),(189,13,'2017-12-03 16:25:00','PIT',0,'NYG',0,0),(190,13,'2017-12-03 16:25:00','SD',0,'TB',0,0),(191,13,'2017-12-03 20:30:00','SEA',0,'CAR',0,0),(192,13,'2017-12-04 20:30:00','NYJ',0,'IND',0,0),(193,14,'2017-12-07 20:25:00','KC',0,'OAK',0,0),(194,14,'2017-12-10 13:00:00','BUF',0,'PIT',0,0),(195,14,'2017-12-10 13:00:00','CAR',0,'SD',0,0),(196,14,'2017-12-10 13:00:00','CLE',0,'CIN',0,0),(197,14,'2017-12-10 13:00:00','DET',0,'CHI',0,0),(198,14,'2017-12-10 13:00:00','IND',0,'HOU',0,0),(199,14,'2017-12-10 13:00:00','JAX',0,'MIN',0,0),(200,14,'2017-12-10 13:00:00','MIA',0,'ARI',0,0),(201,14,'2017-12-10 13:00:00','PHI',0,'WAS',0,0),(202,14,'2017-12-10 13:00:00','TB',0,'NO',0,0),(203,14,'2017-12-10 13:00:00','TEN',0,'DEN',0,0),(204,14,'2017-12-10 16:05:00','SF',0,'NYJ',0,0),(205,14,'2017-12-10 16:25:00','GB',0,'SEA',0,0),(206,14,'2017-12-10 16:25:00','LA',0,'ATL',0,0),(207,14,'2017-12-10 20:30:00','NYG',0,'DAL',0,0),(208,14,'2017-12-11 20:30:00','NE',0,'BAL',0,0),(209,15,'2017-12-14 20:25:00','SEA',0,'LA',0,0),(210,15,'2017-12-16 20:25:00','NYJ',0,'MIA',0,0),(211,15,'2017-12-17 13:00:00','CHI',0,'GB',0,0),(212,15,'2017-12-17 13:00:00','DAL',0,'TB',0,0),(213,15,'2017-12-17 13:00:00','HOU',0,'JAX',0,0),(214,15,'2017-12-17 13:00:00','BUF',0,'CLE',0,0),(215,15,'2017-12-17 13:00:00','BAL',0,'PHI',0,0),(216,15,'2017-12-17 13:00:00','KC',0,'TEN',0,0),(217,15,'2017-12-17 13:00:00','MIN',0,'IND',0,0),(218,15,'2017-12-17 13:00:00','NYG',0,'DET',0,0),(219,15,'2017-12-17 16:05:00','ARI',0,'NO',0,0),(220,15,'2017-12-17 16:05:00','ATL',0,'SF',0,0),(221,15,'2017-12-17 16:25:00','DEN',0,'NE',0,0),(222,15,'2017-12-17 16:25:00','SD',0,'OAK',0,0),(223,15,'2017-12-17 20:30:00','CIN',0,'PIT',0,0),(224,15,'2017-12-18 20:30:00','WAS',0,'CAR',0,0),(225,16,'2017-12-21 20:25:00','PHI',0,'NYG',0,0),(226,16,'2017-12-23 13:00:00','BUF',0,'MIA',0,0),(227,16,'2017-12-23 13:00:00','CAR',0,'ATL',0,0),(228,16,'2017-12-23 13:00:00','CHI',0,'WAS',0,0),(229,16,'2017-12-23 13:00:00','CLE',0,'SD',0,0),(230,16,'2017-12-23 13:00:00','GB',0,'MIN',0,0),(231,16,'2017-12-23 13:00:00','JAX',0,'TEN',0,0),(232,16,'2017-12-23 13:00:00','NE',0,'NYJ',0,0),(233,16,'2017-12-23 13:00:00','NO',0,'TB',0,0),(234,16,'2017-12-23 16:05:00','OAK',0,'IND',0,0),(235,16,'2017-12-23 16:25:00','SEA',0,'ARI',0,0),(236,16,'2017-12-23 16:25:00','LA',0,'SF',0,0),(237,16,'2017-12-23 20:25:00','HOU',0,'CIN',0,0),(238,16,'2017-12-24 16:30:00','PIT',0,'BAL',0,0),(239,16,'2017-12-24 20:30:00','KC',0,'DEN',0,0),(240,16,'2017-12-25 20:30:00','DAL',0,'DET',0,0),(241,17,'2017-12-31 13:00:00','ATL',0,'NO',0,0),(242,17,'2017-12-31 13:00:00','DET',0,'GB',0,0),(243,17,'2017-12-31 13:00:00','IND',0,'JAX',0,0),(244,17,'2017-12-31 13:00:00','MIA',0,'NE',0,0),(245,17,'2017-12-31 13:00:00','MIN',0,'CHI',0,0),(246,17,'2017-12-31 13:00:00','NYJ',0,'BUF',0,0),(247,17,'2017-12-31 13:00:00','PHI',0,'DAL',0,0),(248,17,'2017-12-31 13:00:00','PIT',0,'CLE',0,0),(249,17,'2017-12-31 13:00:00','TB',0,'CAR',0,0),(250,17,'2017-12-31 13:00:00','TEN',0,'HOU',0,0),(251,17,'2017-12-31 13:00:00','WAS',0,'NYG',0,0),(252,17,'2017-12-31 13:00:00','CIN',0,'BAL',0,0),(253,17,'2017-12-31 16:25:00','SF',0,'SEA',0,0),(254,17,'2017-12-31 16:25:00','DEN',0,'OAK',0,0),(255,17,'2017-12-31 16:25:00','LA',0,'ARI',0,0),(256,17,'2017-12-31 16:25:00','SD',0,'KC',0,0);
/*!40000 ALTER TABLE `nflp_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nflp_teams`
--

DROP TABLE IF EXISTS `nflp_teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nflp_teams` (
  `teamID` varchar(10) NOT NULL,
  `divisionID` int(11) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  `team` varchar(50) DEFAULT NULL,
  `displayName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`teamID`),
  KEY `ID` (`teamID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nflp_teams`
--

LOCK TABLES `nflp_teams` WRITE;
/*!40000 ALTER TABLE `nflp_teams` DISABLE KEYS */;
INSERT INTO `nflp_teams` VALUES ('ARI',8,'Arizona','Cardinals',NULL),('ATL',6,'Atlanta','Falcons',NULL),('BAL',1,'Baltimore','Ravens',NULL),('BUF',3,'Buffalo','Bills',NULL),('CAR',6,'Carolina','Panthers',NULL),('CHI',5,'Chicago','Bears',NULL),('CIN',1,'Cincinnati','Bengals',NULL),('CLE',1,'Cleveland','Browns',NULL),('DAL',7,'Dallas','Cowboys',NULL),('DEN',4,'Denver','Broncos',NULL),('DET',5,'Detroit','Lions',NULL),('GB',5,'Green Bay','Packers',NULL),('HOU',2,'Houston','Texans',NULL),('IND',2,'Indianapolis','Colts',NULL),('JAX',2,'Jacksonville','Jaguars',NULL),('KC',4,'Kansas City','Chiefs',NULL),('MIA',3,'Miami','Dolphins',NULL),('MIN',5,'Minnesota','Vikings',NULL),('NE',3,'New England','Patriots',NULL),('NO',6,'New Orleans','Saints',NULL),('NYG',7,'New York','Giants','NY Giants'),('NYJ',3,'New York','Jets','NY Jets'),('OAK',4,'Oakland','Raiders',NULL),('PHI',7,'Philadelphia','Eagles',NULL),('PIT',1,'Pittsburgh','Steelers',NULL),('SD',4,'San Diego','Chargers',NULL),('SEA',8,'Seattle','Seahawks',NULL),('SF',8,'San Francisco','49ers',NULL),('LA',8,'St. Louis','Rams',NULL),('TB',6,'Tampa Bay','Buccaneers',NULL),('TEN',2,'Tennessee','Titans',NULL),('WAS',7,'Washington','Redskins',NULL);
/*!40000 ALTER TABLE `nflp_teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nflp_users`
--

DROP TABLE IF EXISTS `nflp_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nflp_users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(32) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nflp_users`
--

LOCK TABLES `nflp_users` WRITE;
/*!40000 ALTER TABLE `nflp_users` DISABLE KEYS */;
INSERT INTO `nflp_users` VALUES (1,'admin','jl7LZ1B7ZNUq/RnVqnFmuwRXvMkO/DD5','Cb8Jjj0OPy','Admin','Admin','admin@yourdomain.com',1,1),(2,'test','QbPtQkBI6qvnljXk3adU3gFV4YiDsyUX','wD6GJ77H41','Test','Test','test@mail.com',1,0),(3,'tbrusda','JS6jY5s+LfM76ah7mCyci09SAqrFqALoT5zyvx7Rq2NAVu9BawsSYA==','dLFBTvzgdC','Terry','Brusda','tbrusda@professionalfabrications.com',1,0),(4,'mathis31','vIGSN0fgfmhZpMD5fEPxjzWL528QvZ8f','0APZ2Xq8ka','sarah','MATHIS','sarah25950@aol.com',1,0),(5,'bbrusda','dgNsDJ95xG6Tk1ms1qr4tAFV4YiDsyUX','VnDga6AiQV','Test','Test','bdbtzc@gmail.com',1,0);
/*!40000 ALTER TABLE `nflp_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-02  7:34:02
