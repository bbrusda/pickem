-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2015 at 10:11 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nflpickem`
--
CREATE DATABASE IF NOT EXISTS `nflpickem` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `nflpickem`;

-- --------------------------------------------------------

--
-- Table structure for table `nflp_comments`
--

CREATE TABLE IF NOT EXISTS `nflp_comments` (
`commentID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `comment` longtext NOT NULL,
  `postDateTime` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `nflp_comments`
--

INSERT INTO `nflp_comments` (`commentID`, `userID`, `subject`, `comment`, `postDateTime`) VALUES
(1, 2, 'Test123', 'Test', '2015-08-26 16:12:37');

-- --------------------------------------------------------

--
-- Table structure for table `nflp_divisions`
--

CREATE TABLE IF NOT EXISTS `nflp_divisions` (
`divisionID` int(11) NOT NULL,
  `conference` varchar(10) NOT NULL,
  `division` varchar(32) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `nflp_divisions`
--

INSERT INTO `nflp_divisions` (`divisionID`, `conference`, `division`) VALUES
(1, 'AFC', 'North'),
(2, 'AFC', 'South'),
(3, 'AFC', 'East'),
(4, 'AFC', 'West'),
(5, 'NFC', 'North'),
(6, 'NFC', 'South'),
(7, 'NFC', 'East'),
(8, 'NFC', 'West');

-- --------------------------------------------------------

--
-- Table structure for table `nflp_email_templates`
--

CREATE TABLE IF NOT EXISTS `nflp_email_templates` (
  `email_template_key` varchar(255) NOT NULL,
  `email_template_title` varchar(255) NOT NULL,
  `default_subject` varchar(255) DEFAULT NULL,
  `default_message` text,
  `subject` varchar(255) DEFAULT NULL,
  `message` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `nflp_email_templates`
--

INSERT INTO `nflp_email_templates` (`email_template_key`, `email_template_title`, `default_subject`, `default_message`, `subject`, `message`) VALUES
('WEEKLY_PICKS_REMINDER', 'Weekly Picks Reminder', 'NFL Pick ''Em Week {week} Reminder', 'Hello {player},<br /><br />You are receiving this email because you do not yet have all of your picks in for week {week}.&nbsp; This is your reminder.&nbsp; The first game is {first_game} (Eastern), so to receive credit for that game, you''ll have to make your pick before then.<br /><br />Links:<br />&nbsp;- NFL Pick ''Em URL: {site_url}<br />&nbsp;- Help/Rules: {rules_url}<br /><br />Good Luck!<br />', 'NFL Pick ''Em Week {week} Reminder', 'Hello {player},<br /><br />You are receiving this email because you do not yet have all of your picks in for week {week}.&nbsp; This is your reminder.&nbsp; The first game is {first_game} (Eastern), so to receive credit for that game, you''ll have to make your pick before then.<br /><br />Links:<br />&nbsp;- NFL Pick ''Em URL: {site_url}<br />&nbsp;- Help/Rules: {rules_url}<br /><br />Good Luck!<br />'),
('WEEKLY_RESULTS_REMINDER', 'Last Week Results/Reminder', 'NFL Pick ''Em Week {previousWeek} Standings/Reminder', 'Congratulations this week go to {winners} for winning week {previousWeek}.  The winner(s) had {winningScore} out of {possibleScore} picks correct.<br /><br />The current leaders are:<br />{currentLeaders}<br /><br />The most accurate players are:<br />{bestPickRatios}<br /><br />*Reminder* - Please make your picks for week {week} before {first_game} (Eastern).<br /><br />Links:<br />&nbsp;- NFL Pick ''Em URL: {site_url}<br />&nbsp;- Help/Rules: {rules_url}<br /><br />Good Luck!<br />', 'NFL Pick ''Em Week {previousWeek} Standings/Reminder', 'Congratulations this week go to {winners} for winning week {previousWeek}.  The winner(s) had {winningScore} out of {possibleScore} picks correct.<br /><br />The current leaders are:<br />{currentLeaders}<br /><br />The most accurate players are:<br />{bestPickRatios}<br /><br />*Reminder* - Please make your picks for week {week} before {first_game} (Eastern).<br /><br />Links:<br />&nbsp;- NFL Pick ''Em URL: {site_url}<br />&nbsp;- Help/Rules: {rules_url}<br /><br />Good Luck!<br />'),
('FINAL_RESULTS', 'Final Results', 'NFL Pick ''Em 2015 Final Results', 'Congratulations this week go to {winners} for winning week\r\n{previousWeek}. The winner(s) had {winningScore} out of {possibleScore}\r\npicks correct.<br /><br /><span style="font-weight: bold;">Congratulations to {final_winner}</span> for winning NFL Pick ''Em 2015!&nbsp; {final_winner} had {final_winningScore} wins and had a pick ratio of {picks}/{possible} ({pickpercent}%).<br /><br />Top Wins:<br />{currentLeaders}<br /><br />The most accurate players are:<br />{bestPickRatios}<br /><br />Thanks for playing, and I hope to see you all again for NFL Pick ''Em 2012!', 'NFL Pick ''Em 2015 Final Results', 'Congratulations this week go to {winners} for winning week\r\n{previousWeek}. The winner(s) had {winningScore} out of {possibleScore}\r\npicks correct.<br /><br /><span style="font-weight: bold;">Congratulations to {final_winner}</span> for winning NFL Pick ''Em 2015!&nbsp; {final_winner} had {final_winningScore} wins and had a pick ratio of {picks}/{possible} ({pickpercent}%).<br /><br />Top Wins:<br />{currentLeaders}<br /><br />The most accurate players are:<br />{bestPickRatios}<br /><br />Thanks for playing, and I hope to see you all again for NFL Pick ''Em 2012!');

-- --------------------------------------------------------

--
-- Table structure for table `nflp_picks`
--

CREATE TABLE IF NOT EXISTS `nflp_picks` (
  `userID` int(11) NOT NULL,
  `gameID` int(11) NOT NULL,
  `pickID` varchar(10) NOT NULL,
  `points` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `nflp_picksummary`
--

CREATE TABLE IF NOT EXISTS `nflp_picksummary` (
  `weekNum` int(11) NOT NULL DEFAULT '0',
  `userID` int(11) NOT NULL DEFAULT '0',
  `tieBreakerPoints` int(11) NOT NULL,
  `showPicks` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

--
-- Dumping data for table `nflp_picksummary`
--

INSERT INTO `nflp_picksummary` (`weekNum`, `userID`, `tieBreakerPoints`, `showPicks`) VALUES
(1, 2, 0, 1),
(2, 2, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nflp_schedule`
--

CREATE TABLE IF NOT EXISTS `nflp_schedule` (
`gameID` int(11) NOT NULL,
  `weekNum` int(11) NOT NULL,
  `gameTimeEastern` datetime DEFAULT NULL,
  `homeID` varchar(10) NOT NULL,
  `homeScore` int(11) DEFAULT NULL,
  `visitorID` varchar(10) NOT NULL,
  `visitorScore` int(11) DEFAULT NULL,
  `overtime` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=257 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `nflp_schedule`
--

INSERT INTO `nflp_schedule` (`gameID`, `weekNum`, `gameTimeEastern`, `homeID`, `homeScore`, `visitorID`, `visitorScore`, `overtime`) VALUES
(1, 1, '2015-09-10 20:30:00', 'NE', NULL, 'PIT', NULL, 0),
(2, 1, '2015-09-13 13:00:00', 'CHI', NULL, 'GB', NULL, 0),
(3, 1, '2015-09-13 13:00:00', 'HOU', NULL, 'KC', NULL, 0),
(4, 1, '2015-09-13 13:00:00', 'NYJ', NULL, 'CLE', NULL, 0),
(5, 1, '2015-09-13 13:00:00', 'BUF', NULL, 'IND', NULL, 0),
(6, 1, '2015-09-13 13:00:00', 'WAS', NULL, 'MIA', NULL, 0),
(7, 1, '2015-09-13 13:00:00', 'JAX', NULL, 'CAR', NULL, 0),
(8, 1, '2015-09-13 13:00:00', 'STL', NULL, 'SEA', NULL, 0),
(9, 1, '2015-09-13 16:05:00', 'ARI', NULL, 'NO', NULL, 0),
(10, 1, '2015-09-13 16:05:00', 'SD', NULL, 'DET', NULL, 0),
(11, 1, '2015-09-13 16:25:00', 'TB', NULL, 'TEN', NULL, 0),
(12, 1, '2015-09-13 16:25:00', 'OAK', NULL, 'CIN', NULL, 0),
(13, 1, '2015-09-13 16:25:00', 'DEN', NULL, 'BAL', NULL, 0),
(14, 1, '2015-09-13 20:30:00', 'DAL', NULL, 'NYG', NULL, 0),
(15, 1, '2015-09-14 19:10:00', 'ATL', NULL, 'PHI', NULL, 0),
(16, 1, '2015-09-14 22:20:00', 'SF', NULL, 'MIN', NULL, 0),
(17, 2, '2015-09-17 20:25:00', 'KC', 13, 'DEN', 12, 0),
(18, 2, '2015-09-20 13:00:00', 'CAR', 13, 'HOU', 12, 0),
(19, 2, '2015-09-20 13:00:00', 'PIT', 13, 'SF', 12, 0),
(20, 2, '2015-09-20 13:00:00', 'NO', 13, 'TB', 12, 0),
(21, 2, '2015-09-20 13:00:00', 'MIN', 13, 'DET', 12, 0),
(22, 2, '2015-09-20 13:00:00', 'CHI', 13, 'ARI', 12, 0),
(23, 2, '2015-09-20 13:00:00', 'BUF', 312, 'NE', 12, 0),
(24, 2, '2015-09-20 13:00:00', 'CIN', 13, 'SD', 12, 0),
(25, 2, '2015-09-20 13:00:00', 'CLE', 13, 'TEN', 12, 0),
(26, 2, '2015-09-20 13:00:00', 'NYG', 12, 'ATL', 13, 0),
(27, 2, '2015-09-20 13:00:00', 'WAS', 13, 'STL', 12, 0),
(28, 2, '2015-09-20 16:05:00', 'JAX', 123, 'MIA', 13, 0),
(29, 2, '2015-09-20 16:05:00', 'OAK', 13, 'BAL', 12, 0),
(30, 2, '2015-09-20 16:25:00', 'PHI', 123, 'DAL', 123, 0),
(31, 2, '2015-09-20 20:30:00', 'GB', 123, 'SEA', 123, 0),
(32, 2, '2015-09-21 20:30:00', 'IND', 123, 'NYJ', 123, 0),
(33, 3, '2015-09-24 20:25:00', 'NYG', NULL, 'WAS', NULL, 0),
(34, 3, '2015-09-27 13:00:00', 'DAL', NULL, 'ATL', NULL, 0),
(35, 3, '2015-09-27 13:00:00', 'TEN', NULL, 'IND', NULL, 0),
(36, 3, '2015-09-27 13:00:00', 'CLE', NULL, 'OAK', NULL, 0),
(37, 3, '2015-09-27 13:00:00', 'BAL', NULL, 'CIN', NULL, 0),
(38, 3, '2015-09-27 13:00:00', 'NE', NULL, 'JAX', NULL, 0),
(39, 3, '2015-09-27 13:00:00', 'CAR', NULL, 'NO', NULL, 0),
(40, 3, '2015-09-27 13:00:00', 'NYJ', NULL, 'PHI', NULL, 0),
(41, 3, '2015-09-27 13:00:00', 'HOU', NULL, 'TB', NULL, 0),
(42, 3, '2015-09-27 13:00:00', 'MIN', NULL, 'SD', NULL, 0),
(43, 3, '2015-09-27 13:00:00', 'STL', NULL, 'PIT', NULL, 0),
(44, 3, '2015-09-27 16:05:00', 'ARI', NULL, 'SF', NULL, 0),
(45, 3, '2015-09-27 16:25:00', 'MIA', NULL, 'BUF', NULL, 0),
(46, 3, '2015-09-27 16:25:00', 'SEA', NULL, 'CHI', NULL, 0),
(47, 3, '2015-09-27 20:30:00', 'DET', NULL, 'DEN', NULL, 0),
(48, 3, '2015-09-28 20:30:00', 'GB', NULL, 'KC', NULL, 0),
(49, 4, '2015-10-01 20:25:00', 'PIT', NULL, 'BAL', NULL, 0),
(50, 4, '2015-10-04 09:30:00', 'MIA', NULL, 'NYJ', NULL, 0),
(51, 4, '2015-10-04 13:00:00', 'IND', NULL, 'JAX', NULL, 0),
(52, 4, '2015-10-04 13:00:00', 'BUF', NULL, 'NYG', NULL, 0),
(53, 4, '2015-10-04 13:00:00', 'TB', NULL, 'CAR', NULL, 0),
(54, 4, '2015-10-04 13:00:00', 'WAS', NULL, 'PHI', NULL, 0),
(55, 4, '2015-10-04 13:00:00', 'CHI', NULL, 'OAK', NULL, 0),
(56, 4, '2015-10-04 13:00:00', 'ATL', NULL, 'HOU', NULL, 0),
(57, 4, '2015-10-04 13:00:00', 'CIN', NULL, 'KC', NULL, 0),
(58, 4, '2015-10-04 16:05:00', 'SD', NULL, 'CLE', NULL, 0),
(59, 4, '2015-10-04 16:25:00', 'SF', NULL, 'GB', NULL, 0),
(60, 4, '2015-10-04 16:25:00', 'ARI', NULL, 'STL', NULL, 0),
(61, 4, '2015-10-04 16:25:00', 'DEN', NULL, 'MIN', NULL, 0),
(62, 4, '2015-10-04 20:30:00', 'NO', NULL, 'DAL', NULL, 0),
(63, 4, '2015-10-05 20:30:00', 'SEA', NULL, 'DET', NULL, 0),
(64, 5, '2015-10-08 20:25:00', 'HOU', NULL, 'IND', NULL, 0),
(65, 5, '2015-10-11 13:00:00', 'KC', NULL, 'CHI', NULL, 0),
(66, 5, '2015-10-11 13:00:00', 'CIN', NULL, 'SEA', NULL, 0),
(67, 5, '2015-10-11 13:00:00', 'ATL', NULL, 'WAS', NULL, 0),
(68, 5, '2015-10-11 13:00:00', 'TB', NULL, 'JAX', NULL, 0),
(69, 5, '2015-10-11 13:00:00', 'PHI', NULL, 'NO', NULL, 0),
(70, 5, '2015-10-11 13:00:00', 'BAL', NULL, 'CLE', NULL, 0),
(71, 5, '2015-10-11 13:00:00', 'GB', NULL, 'STL', NULL, 0),
(72, 5, '2015-10-11 13:00:00', 'TEN', NULL, 'BUF', NULL, 0),
(73, 5, '2015-10-11 16:05:00', 'DET', NULL, 'ARI', NULL, 0),
(74, 5, '2015-10-11 16:25:00', 'DAL', NULL, 'NE', NULL, 0),
(75, 5, '2015-10-11 16:25:00', 'OAK', NULL, 'DEN', NULL, 0),
(76, 5, '2015-10-11 20:30:00', 'NYG', NULL, 'SF', NULL, 0),
(77, 5, '2015-10-12 20:30:00', 'SD', NULL, 'PIT', NULL, 0),
(78, 6, '2015-10-15 20:25:00', 'NO', NULL, 'ATL', NULL, 0),
(79, 6, '2015-10-18 13:00:00', 'NYJ', NULL, 'WAS', NULL, 0),
(80, 6, '2015-10-18 13:00:00', 'PIT', NULL, 'ARI', NULL, 0),
(81, 6, '2015-10-18 13:00:00', 'MIN', NULL, 'KC', NULL, 0),
(82, 6, '2015-10-18 13:00:00', 'BUF', NULL, 'CIN', NULL, 0),
(83, 6, '2015-10-18 13:00:00', 'DET', NULL, 'CHI', NULL, 0),
(84, 6, '2015-10-18 13:00:00', 'CLE', NULL, 'DEN', NULL, 0),
(85, 6, '2015-10-18 13:00:00', 'JAX', NULL, 'HOU', NULL, 0),
(86, 6, '2015-10-18 13:00:00', 'TEN', NULL, 'MIA', NULL, 0),
(87, 6, '2015-10-18 16:05:00', 'SEA', NULL, 'CAR', NULL, 0),
(88, 6, '2015-10-18 16:25:00', 'GB', NULL, 'SD', NULL, 0),
(89, 6, '2015-10-18 16:25:00', 'SF', NULL, 'BAL', NULL, 0),
(90, 6, '2015-10-18 20:30:00', 'IND', NULL, 'NE', NULL, 0),
(91, 6, '2015-10-19 20:30:00', 'PHI', NULL, 'NYG', NULL, 0),
(92, 7, '2015-10-22 20:25:00', 'SF', NULL, 'SEA', NULL, 0),
(93, 7, '2015-10-25 09:30:00', 'JAX', NULL, 'BUF', NULL, 0),
(94, 7, '2015-10-25 13:00:00', 'WAS', NULL, 'TB', NULL, 0),
(95, 7, '2015-10-25 13:00:00', 'TEN', NULL, 'ATL', NULL, 0),
(96, 7, '2015-10-25 13:00:00', 'IND', NULL, 'NO', NULL, 0),
(97, 7, '2015-10-25 13:00:00', 'DET', NULL, 'MIN', NULL, 0),
(98, 7, '2015-10-25 13:00:00', 'KC', NULL, 'PIT', NULL, 0),
(99, 7, '2015-10-25 13:00:00', 'STL', NULL, 'CLE', NULL, 0),
(100, 7, '2015-10-25 13:00:00', 'MIA', NULL, 'HOU', NULL, 0),
(101, 7, '2015-10-25 13:00:00', 'NE', NULL, 'NYJ', NULL, 0),
(102, 7, '2015-10-25 16:05:00', 'SD', NULL, 'OAK', NULL, 0),
(103, 7, '2015-10-25 16:25:00', 'NYG', NULL, 'DAL', NULL, 0),
(104, 7, '2015-10-25 20:30:00', 'CAR', NULL, 'PHI', NULL, 0),
(105, 7, '2015-10-26 20:30:00', 'ARI', NULL, 'BAL', NULL, 0),
(106, 8, '2015-10-29 20:25:00', 'NE', NULL, 'MIA', NULL, 0),
(107, 8, '2015-11-01 09:30:00', 'KC', NULL, 'DET', NULL, 0),
(108, 8, '2015-11-01 13:00:00', 'ATL', NULL, 'TB', NULL, 0),
(109, 8, '2015-11-01 13:00:00', 'CLE', NULL, 'ARI', NULL, 0),
(110, 8, '2015-11-01 13:00:00', 'STL', NULL, 'SF', NULL, 0),
(111, 8, '2015-11-01 13:00:00', 'NO', NULL, 'NYG', NULL, 0),
(112, 8, '2015-11-01 13:00:00', 'CHI', NULL, 'MIN', NULL, 0),
(113, 8, '2015-11-01 13:00:00', 'BAL', NULL, 'SD', NULL, 0),
(114, 8, '2015-11-01 13:00:00', 'PIT', NULL, 'CIN', NULL, 0),
(115, 8, '2015-11-01 13:00:00', 'HOU', NULL, 'TEN', NULL, 0),
(116, 8, '2015-11-01 16:05:00', 'OAK', NULL, 'NYJ', NULL, 0),
(117, 8, '2015-11-01 16:25:00', 'DAL', NULL, 'SEA', NULL, 0),
(118, 8, '2015-11-01 20:30:00', 'DEN', NULL, 'GB', NULL, 0),
(119, 8, '2015-11-02 20:30:00', 'CAR', NULL, 'IND', NULL, 0),
(120, 9, '2015-11-05 20:25:00', 'CIN', NULL, 'CLE', NULL, 0),
(121, 9, '2015-11-08 13:00:00', 'CAR', NULL, 'GB', NULL, 0),
(122, 9, '2015-11-08 13:00:00', 'NE', NULL, 'WAS', NULL, 0),
(123, 9, '2015-11-08 13:00:00', 'NO', NULL, 'TEN', NULL, 0),
(124, 9, '2015-11-08 13:00:00', 'BUF', NULL, 'MIA', NULL, 0),
(125, 9, '2015-11-08 13:00:00', 'MIN', NULL, 'STL', NULL, 0),
(126, 9, '2015-11-08 13:00:00', 'NYJ', NULL, 'JAX', NULL, 0),
(127, 9, '2015-11-08 13:00:00', 'PIT', NULL, 'OAK', NULL, 0),
(128, 9, '2015-11-08 16:05:00', 'TB', NULL, 'NYG', NULL, 0),
(129, 9, '2015-11-08 16:05:00', 'SF', NULL, 'ATL', NULL, 0),
(130, 9, '2015-11-08 16:25:00', 'IND', NULL, 'DEN', NULL, 0),
(131, 9, '2015-11-08 20:30:00', 'DAL', NULL, 'PHI', NULL, 0),
(132, 9, '2015-11-09 20:30:00', 'SD', NULL, 'CHI', NULL, 0),
(133, 10, '2015-11-12 20:25:00', 'NYJ', NULL, 'BUF', NULL, 0),
(134, 10, '2015-11-15 13:00:00', 'GB', NULL, 'DET', NULL, 0),
(135, 10, '2015-11-15 13:00:00', 'TB', NULL, 'DAL', NULL, 0),
(136, 10, '2015-11-15 13:00:00', 'TEN', NULL, 'CAR', NULL, 0),
(137, 10, '2015-11-15 13:00:00', 'STL', NULL, 'CHI', NULL, 0),
(138, 10, '2015-11-15 13:00:00', 'WAS', NULL, 'NO', NULL, 0),
(139, 10, '2015-11-15 13:00:00', 'PHI', NULL, 'MIA', NULL, 0),
(140, 10, '2015-11-15 13:00:00', 'PIT', NULL, 'CLE', NULL, 0),
(141, 10, '2015-11-15 13:00:00', 'BAL', NULL, 'JAX', NULL, 0),
(142, 10, '2015-11-15 16:05:00', 'OAK', NULL, 'MIN', NULL, 0),
(143, 10, '2015-11-15 16:25:00', 'NYG', NULL, 'NE', NULL, 0),
(144, 10, '2015-11-15 16:25:00', 'DEN', NULL, 'KC', NULL, 0),
(145, 10, '2015-11-15 20:30:00', 'SEA', NULL, 'ARI', NULL, 0),
(146, 10, '2015-11-16 20:30:00', 'CIN', NULL, 'HOU', NULL, 0),
(147, 11, '2015-11-19 20:25:00', 'JAX', NULL, 'TEN', NULL, 0),
(148, 11, '2015-11-22 13:00:00', 'DET', NULL, 'OAK', NULL, 0),
(149, 11, '2015-11-22 13:00:00', 'ATL', NULL, 'IND', NULL, 0),
(150, 11, '2015-11-22 13:00:00', 'HOU', NULL, 'NYJ', NULL, 0),
(151, 11, '2015-11-22 13:00:00', 'PHI', NULL, 'TB', NULL, 0),
(152, 11, '2015-11-22 13:00:00', 'CHI', NULL, 'DEN', NULL, 0),
(153, 11, '2015-11-22 13:00:00', 'MIN', NULL, 'GB', NULL, 0),
(154, 11, '2015-11-22 13:00:00', 'BAL', NULL, 'STL', NULL, 0),
(155, 11, '2015-11-22 13:00:00', 'MIA', NULL, 'DAL', NULL, 0),
(156, 11, '2015-11-22 13:00:00', 'CAR', NULL, 'WAS', NULL, 0),
(157, 11, '2015-11-22 16:05:00', 'ARI', NULL, 'CIN', NULL, 0),
(158, 11, '2015-11-22 16:25:00', 'SEA', NULL, 'SF', NULL, 0),
(159, 11, '2015-11-22 20:30:00', 'SD', NULL, 'KC', NULL, 0),
(160, 11, '2015-11-23 20:30:00', 'NE', NULL, 'BUF', NULL, 0),
(161, 12, '2015-11-26 12:30:00', 'DET', NULL, 'PHI', NULL, 0),
(162, 12, '2015-11-26 16:30:00', 'DAL', NULL, 'CAR', NULL, 0),
(163, 12, '2015-11-26 20:30:00', 'GB', NULL, 'CHI', NULL, 0),
(164, 12, '2015-11-29 13:00:00', 'TEN', NULL, 'OAK', NULL, 0),
(165, 12, '2015-11-29 13:00:00', 'KC', NULL, 'BUF', NULL, 0),
(166, 12, '2015-11-29 13:00:00', 'IND', NULL, 'TB', NULL, 0),
(167, 12, '2015-11-29 13:00:00', 'WAS', NULL, 'NYG', NULL, 0),
(168, 12, '2015-11-29 13:00:00', 'HOU', NULL, 'NO', NULL, 0),
(169, 12, '2015-11-29 13:00:00', 'ATL', NULL, 'MIN', NULL, 0),
(170, 12, '2015-11-29 13:00:00', 'CIN', NULL, 'STL', NULL, 0),
(171, 12, '2015-11-29 13:00:00', 'JAX', NULL, 'SD', NULL, 0),
(172, 12, '2015-11-29 13:00:00', 'NYJ', NULL, 'MIA', NULL, 0),
(173, 12, '2015-11-29 16:05:00', 'SF', NULL, 'ARI', NULL, 0),
(174, 12, '2015-11-29 16:25:00', 'SEA', NULL, 'PIT', NULL, 0),
(175, 12, '2015-11-29 20:30:00', 'DEN', NULL, 'NE', NULL, 0),
(176, 12, '2015-11-30 20:30:00', 'CLE', NULL, 'BAL', NULL, 0),
(177, 13, '2015-12-03 20:25:00', 'DET', NULL, 'GB', NULL, 0),
(178, 13, '2015-12-06 13:00:00', 'NYG', NULL, 'NYJ', NULL, 0),
(179, 13, '2015-12-06 13:00:00', 'STL', NULL, 'ARI', NULL, 0),
(180, 13, '2015-12-06 13:00:00', 'TB', NULL, 'ATL', NULL, 0),
(181, 13, '2015-12-06 13:00:00', 'NO', NULL, 'CAR', NULL, 0),
(182, 13, '2015-12-06 13:00:00', 'MIN', NULL, 'SEA', NULL, 0),
(183, 13, '2015-12-06 13:00:00', 'BUF', NULL, 'HOU', NULL, 0),
(184, 13, '2015-12-06 13:00:00', 'MIA', NULL, 'BAL', NULL, 0),
(185, 13, '2015-12-06 13:00:00', 'CLE', NULL, 'CIN', NULL, 0),
(186, 13, '2015-12-06 13:00:00', 'TEN', NULL, 'JAX', NULL, 0),
(187, 13, '2015-12-06 13:00:00', 'CHI', NULL, 'SF', NULL, 0),
(188, 13, '2015-12-06 16:05:00', 'SD', NULL, 'DEN', NULL, 0),
(189, 13, '2015-12-06 16:05:00', 'OAK', NULL, 'KC', NULL, 0),
(190, 13, '2015-12-06 16:25:00', 'NE', NULL, 'PHI', NULL, 0),
(191, 13, '2015-12-06 20:30:00', 'PIT', NULL, 'IND', NULL, 0),
(192, 13, '2015-12-07 20:30:00', 'WAS', NULL, 'DAL', NULL, 0),
(193, 14, '2015-12-10 20:25:00', 'ARI', NULL, 'MIN', NULL, 0),
(194, 14, '2015-12-13 13:00:00', 'PHI', NULL, 'BUF', NULL, 0),
(195, 14, '2015-12-13 13:00:00', 'CLE', NULL, 'SF', NULL, 0),
(196, 14, '2015-12-13 13:00:00', 'STL', NULL, 'DET', NULL, 0),
(197, 14, '2015-12-13 13:00:00', 'TB', NULL, 'NO', NULL, 0),
(198, 14, '2015-12-13 13:00:00', 'NYJ', NULL, 'TEN', NULL, 0),
(199, 14, '2015-12-13 13:00:00', 'CIN', NULL, 'PIT', NULL, 0),
(200, 14, '2015-12-13 13:00:00', 'HOU', NULL, 'NE', NULL, 0),
(201, 14, '2015-12-13 13:00:00', 'JAX', NULL, 'IND', NULL, 0),
(202, 14, '2015-12-13 13:00:00', 'KC', NULL, 'SD', NULL, 0),
(203, 14, '2015-12-13 13:00:00', 'CHI', NULL, 'WAS', NULL, 0),
(204, 14, '2015-12-13 13:00:00', 'CAR', NULL, 'ATL', NULL, 0),
(205, 14, '2015-12-13 16:05:00', 'DEN', NULL, 'OAK', NULL, 0),
(206, 14, '2015-12-13 16:25:00', 'GB', NULL, 'DAL', NULL, 0),
(207, 14, '2015-12-13 20:30:00', 'BAL', NULL, 'SEA', NULL, 0),
(208, 14, '2015-12-14 20:30:00', 'MIA', NULL, 'NYG', NULL, 0),
(209, 15, '2015-12-17 20:25:00', 'STL', NULL, 'TB', NULL, 0),
(210, 15, '2015-12-19 20:25:00', 'DAL', NULL, 'NYJ', NULL, 0),
(211, 15, '2015-12-20 13:00:00', 'MIN', NULL, 'CHI', NULL, 0),
(212, 15, '2015-12-20 13:00:00', 'JAX', NULL, 'ATL', NULL, 0),
(213, 15, '2015-12-20 13:00:00', 'IND', NULL, 'HOU', NULL, 0),
(214, 15, '2015-12-20 13:00:00', 'PHI', NULL, 'ARI', NULL, 0),
(215, 15, '2015-12-20 13:00:00', 'NYG', NULL, 'CAR', NULL, 0),
(216, 15, '2015-12-20 13:00:00', 'NE', NULL, 'TEN', NULL, 0),
(217, 15, '2015-12-20 13:00:00', 'WAS', NULL, 'BUF', NULL, 0),
(218, 15, '2015-12-20 13:00:00', 'BAL', NULL, 'KC', NULL, 0),
(219, 15, '2015-12-20 16:05:00', 'SEA', NULL, 'CLE', NULL, 0),
(220, 15, '2015-12-20 16:05:00', 'OAK', NULL, 'GB', NULL, 0),
(221, 15, '2015-12-20 16:25:00', 'PIT', NULL, 'DEN', NULL, 0),
(222, 15, '2015-12-20 16:25:00', 'SD', NULL, 'MIA', NULL, 0),
(223, 15, '2015-12-20 20:30:00', 'SF', NULL, 'CIN', NULL, 0),
(224, 15, '2015-12-21 20:30:00', 'NO', NULL, 'DET', NULL, 0),
(225, 16, '2015-12-24 20:25:00', 'OAK', NULL, 'SD', NULL, 0),
(226, 16, '2015-12-26 20:25:00', 'PHI', NULL, 'WAS', NULL, 0),
(227, 16, '2015-12-27 13:00:00', 'NYJ', NULL, 'NE', NULL, 0),
(228, 16, '2015-12-27 13:00:00', 'TEN', NULL, 'HOU', NULL, 0),
(229, 16, '2015-12-27 13:00:00', 'KC', NULL, 'CLE', NULL, 0),
(230, 16, '2015-12-27 13:00:00', 'MIA', NULL, 'IND', NULL, 0),
(231, 16, '2015-12-27 13:00:00', 'NO', NULL, 'JAX', NULL, 0),
(232, 16, '2015-12-27 13:00:00', 'DET', NULL, 'SF', NULL, 0),
(233, 16, '2015-12-27 13:00:00', 'BUF', NULL, 'DAL', NULL, 0),
(234, 16, '2015-12-27 13:00:00', 'TB', NULL, 'CHI', NULL, 0),
(235, 16, '2015-12-27 13:00:00', 'ATL', NULL, 'CAR', NULL, 0),
(236, 16, '2015-12-27 13:00:00', 'MIN', NULL, 'NYG', NULL, 0),
(237, 16, '2015-12-27 16:25:00', 'SEA', NULL, 'STL', NULL, 0),
(238, 16, '2015-12-27 16:25:00', 'ARI', NULL, 'GB', NULL, 0),
(239, 16, '2015-12-27 20:30:00', 'BAL', NULL, 'PIT', NULL, 0),
(240, 16, '2015-12-28 20:30:00', 'DEN', NULL, 'CIN', NULL, 0),
(241, 17, '2016-01-03 13:00:00', 'BUF', NULL, 'NYJ', NULL, 0),
(242, 17, '2016-01-03 13:00:00', 'MIA', NULL, 'NE', NULL, 0),
(243, 17, '2016-01-03 13:00:00', 'CAR', NULL, 'TB', NULL, 0),
(244, 17, '2016-01-03 13:00:00', 'ATL', NULL, 'NO', NULL, 0),
(245, 17, '2016-01-03 13:00:00', 'CIN', NULL, 'BAL', NULL, 0),
(246, 17, '2016-01-03 13:00:00', 'CLE', NULL, 'PIT', NULL, 0),
(247, 17, '2016-01-03 13:00:00', 'HOU', NULL, 'JAX', NULL, 0),
(248, 17, '2016-01-03 13:00:00', 'IND', NULL, 'TEN', NULL, 0),
(249, 17, '2016-01-03 13:00:00', 'KC', NULL, 'OAK', NULL, 0),
(250, 17, '2016-01-03 13:00:00', 'DAL', NULL, 'WAS', NULL, 0),
(251, 17, '2016-01-03 13:00:00', 'NYG', NULL, 'PHI', NULL, 0),
(252, 17, '2016-01-03 13:00:00', 'CHI', NULL, 'DET', NULL, 0),
(253, 17, '2016-01-03 13:00:00', 'GB', NULL, 'MIN', NULL, 0),
(254, 17, '2016-01-03 16:25:00', 'DEN', NULL, 'SD', NULL, 0),
(255, 17, '2016-01-03 16:25:00', 'ARI', NULL, 'SEA', NULL, 0),
(256, 17, '2016-01-03 16:25:00', 'SF', NULL, 'STL', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `nflp_teams`
--

CREATE TABLE IF NOT EXISTS `nflp_teams` (
  `teamID` varchar(10) NOT NULL,
  `divisionID` int(11) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  `team` varchar(50) DEFAULT NULL,
  `displayName` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `nflp_teams`
--

INSERT INTO `nflp_teams` (`teamID`, `divisionID`, `city`, `team`, `displayName`) VALUES
('ARI', 8, 'Arizona', 'Cardinals', NULL),
('ATL', 6, 'Atlanta', 'Falcons', NULL),
('BAL', 1, 'Baltimore', 'Ravens', NULL),
('BUF', 3, 'Buffalo', 'Bills', NULL),
('CAR', 6, 'Carolina', 'Panthers', NULL),
('CHI', 5, 'Chicago', 'Bears', NULL),
('CIN', 1, 'Cincinnati', 'Bengals', NULL),
('CLE', 1, 'Cleveland', 'Browns', NULL),
('DAL', 7, 'Dallas', 'Cowboys', NULL),
('DEN', 4, 'Denver', 'Broncos', NULL),
('DET', 5, 'Detroit', 'Lions', NULL),
('GB', 5, 'Green Bay', 'Packers', NULL),
('HOU', 2, 'Houston', 'Texans', NULL),
('IND', 2, 'Indianapolis', 'Colts', NULL),
('JAX', 2, 'Jacksonville', 'Jaguars', NULL),
('KC', 4, 'Kansas City', 'Chiefs', NULL),
('MIA', 3, 'Miami', 'Dolphins', NULL),
('MIN', 5, 'Minnesota', 'Vikings', NULL),
('NE', 3, 'New England', 'Patriots', NULL),
('NO', 6, 'New Orleans', 'Saints', NULL),
('NYG', 7, 'New York', 'Giants', 'NY Giants'),
('NYJ', 3, 'New York', 'Jets', 'NY Jets'),
('OAK', 4, 'Oakland', 'Raiders', NULL),
('PHI', 7, 'Philadelphia', 'Eagles', NULL),
('PIT', 1, 'Pittsburgh', 'Steelers', NULL),
('SD', 4, 'San Diego', 'Chargers', NULL),
('SEA', 8, 'Seattle', 'Seahawks', NULL),
('SF', 8, 'San Francisco', '49ers', NULL),
('STL', 8, 'St. Louis', 'Rams', NULL),
('TB', 6, 'Tampa Bay', 'Buccaneers', NULL),
('TEN', 2, 'Tennessee', 'Titans', NULL),
('WAS', 7, 'Washington', 'Redskins', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `nflp_users`
--

CREATE TABLE IF NOT EXISTS `nflp_users` (
`userID` int(11) NOT NULL,
  `userName` varchar(32) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `nflp_users`
--

INSERT INTO `nflp_users` (`userID`, `userName`, `password`, `salt`, `firstname`, `lastname`, `email`, `status`, `is_admin`) VALUES
(1, 'admin', 'jl7LZ1B7ZNUq/RnVqnFmuwRXvMkO/DD5', 'Cb8Jjj0OPy', 'Admin', 'Admin', 'admin@yourdomain.com', 1, 1),
(2, 'test', 'QbPtQkBI6qvnljXk3adU3gFV4YiDsyUX', 'wD6GJ77H41', 'Test', 'Test', 'test@mail.com', 1, 0),
(3, 'tbrusda', 'JS6jY5s+LfM76ah7mCyci09SAqrFqALoT5zyvx7Rq2NAVu9BawsSYA==', 'dLFBTvzgdC', 'Terry', 'Brusda', 'tbrusda@professionalfabrications.com', 1, 0),
(4, 'mathis31', 'vIGSN0fgfmhZpMD5fEPxjzWL528QvZ8f', '0APZ2Xq8ka', 'sarah', 'MATHIS', 'sarah25950@aol.com', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `nflp_comments`
--
ALTER TABLE `nflp_comments`
 ADD PRIMARY KEY (`commentID`);

--
-- Indexes for table `nflp_divisions`
--
ALTER TABLE `nflp_divisions`
 ADD PRIMARY KEY (`divisionID`);

--
-- Indexes for table `nflp_email_templates`
--
ALTER TABLE `nflp_email_templates`
 ADD PRIMARY KEY (`email_template_key`);

--
-- Indexes for table `nflp_picks`
--
ALTER TABLE `nflp_picks`
 ADD PRIMARY KEY (`userID`,`gameID`);

--
-- Indexes for table `nflp_picksummary`
--
ALTER TABLE `nflp_picksummary`
 ADD PRIMARY KEY (`weekNum`,`userID`);

--
-- Indexes for table `nflp_schedule`
--
ALTER TABLE `nflp_schedule`
 ADD PRIMARY KEY (`gameID`), ADD KEY `GameID` (`gameID`), ADD KEY `HomeID` (`homeID`), ADD KEY `VisitorID` (`visitorID`);

--
-- Indexes for table `nflp_teams`
--
ALTER TABLE `nflp_teams`
 ADD PRIMARY KEY (`teamID`), ADD KEY `ID` (`teamID`);

--
-- Indexes for table `nflp_users`
--
ALTER TABLE `nflp_users`
 ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nflp_comments`
--
ALTER TABLE `nflp_comments`
MODIFY `commentID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `nflp_divisions`
--
ALTER TABLE `nflp_divisions`
MODIFY `divisionID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `nflp_schedule`
--
ALTER TABLE `nflp_schedule`
MODIFY `gameID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=257;
--
-- AUTO_INCREMENT for table `nflp_users`
--
ALTER TABLE `nflp_users`
MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
