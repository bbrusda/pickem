<?php
@include_once 'kint/Kint.class.php';
?>
<p class="skip2content"><a href="<?php echo $_SERVER['REQUEST_URI']; ?>#content">Skip to content &raquo;</a></p>

<!--<div class="bg-primary">
			<b>Current Time (Eastern):</b><br />
			<span id="jclock1"></span>
			<script type="text/javascript">
			$(function($) {
				var optionsEST = {
			        timeNotation: '12h',
			        am_pm: true,
					utc: true,
					utc_offset: -<?php echo 4 + SERVER_TIMEZONE_OFFSET; ?>
				}
				$('#jclock1').jclock(optionsEST);
		    });
			</script>
		</div>-->

<!-- start countdown code - http://keith-wood.name/countdown.html -->
<?php
if ($firstGameTime !== $cutoffDateTime && !$firstGameExpired) {
    ?>
    <div id="firstGame" class="countdown bg-success"></div>
    <script type="text/javascript">
        //set up countdown for first game
        var firstGameTime = new Date("<?php echo date('F j, Y H:i:00', strtotime($firstGameTime)); ?>");
        firstGameTime.setHours(firstGameTime.getHours() - 1);
        $('#firstGame').countdown({until: firstGameTime, description: 'until first game is locked'});
    </script>
    <?php
}
if (!$weekExpired) {
    ?>
    <div id="picksLocked" class="countdown bg-danger"></div>
    <script type="text/javascript">
        //set up countdown for picks lock time
        var picksLockedTime = new Date("<?php echo date('F j, Y H:i:00', strtotime($cutoffDateTime)); ?>");
        picksLockedTime.setHours(picksLockedTime.getHours() - 1);
        $('#picksLocked').countdown({
            until: picksLockedTime,
            description: 'until week <?php echo $currentWeek; ?> is started'
        });
    </script>
    <?php
} else {
    //current week is expired
}
if (strpos($_SERVER['REQUEST_URI'], "entry.php") !== false) {
    $locked_ids = array();
    $query = $mysqli->query($sql_1) or die($mysqli->error);
    $num_rows = ($query->num_rows);
    $picks = getUserPicks($week, $user->userID);

    if ($num_rows > 0) {

        $i = 1;
        $games = array();
        $locked_games = array();
        $locked_ids = setLockedIds();
        while ($row = $query->fetch_assoc()) {
            $dates_passed = false;
            if (strtotime('now') > strtotime($row['gameTimeEastern'])) $dates_passed = true;
            if($dates_passed && !in_array($row['gameID'], $locked_ids)) {
                $sql = "insert into " . DB_PREFIX . "picks (userID, gameID, points) values (" . $user->userID . ", " . $row['gameID'] . ", ".getHighPts($num_rows).")";
                $mysqli->query($sql) or die('Error inserting pick: ' . $mysqli->error);
            }
        }
        $picks = getUserPicks($week, $user->userID);
        foreach ($picks as $pick) {
            $matchups[$pick['points']] = $pick;
            if ($pick['datePassed'] || strtotime('now') > strtotime(getCutoffDateTime($week))) {
                if(!in_array($pick['gameID'], $locked_ids))
                    $locked_ids[] = $pick['gameID'];
                $locked_games[] = $pick;
            }
            else
                $games[] = $pick['gameID'];

        }
        $query = $mysqli->query($sql_1) or die($mysqli->error);
        while ($row = $query->fetch_assoc()) {
            $i = 1;
            while (!is_null($matchups[$i])) {
                $i++;
            }
            $dates_passed = false;
            if (strtotime('now') > strtotime(getCutoffDateTime($week))) $dates_passed = true;
            if (!in_array($row['gameID'], $games) && !in_array($row['gameID'], $locked_ids) && !$dates_passed)
                $matchups[$i] = array('datePassed' => $dates_passed, 'gameID' => $row['gameID'], 'homeID' => $row['homeID'], 'visitorID' => $row['visitorID']);
            if($dates_passed && !in_array($row['gameID'], $locked_ids)) {

                $locked_games[] = array('datePassed' => $dates_passed, 'gameID' => $row['gameID'], 'homeID' => $row['homeID'], 'visitorID' => $row['visitorID']);
            }


            $i++;
        }

        $row = null;
        if (count($locked_games) == 0)
            echo '<p class="bg-success"><b>Drag Games Below to Adjust Points Values!</b><br />';
        else if (count($locked_games) > 0)
            echo '<p class="bg-success"><b>This Weeks Selections</b><br />';
        echo '<div id="sticky-anchor"></div>';
        echo '<div id="sticky">';

        echo '<ul id="unsortable">';

        foreach($locked_games as $row){
            echo '<li class="ui-state-disabled-game" data-points="'.$row['points'].'">'.$row['points'];
            echo '<span class="right-matchup">';
            if($row['pickID'] == $row['homeID'])
                echo '<b class="winner-color">' . $row['homeID'] . '</b>';
            else{
                echo '<b class="loser-color">' . $row['homeID'] . '</b>';
            }
            echo'</span>';
            echo '<img class="logo-right" src="images/logos/' . $row['homeID'] . '.svg" />';
            echo '<span class="vs"> vs </span>';
            echo '<img class="logo-right2" src="images/logos/' . $row['visitorID'] . '.svg" />';
            echo '<span class="right-matchup2">';
            if($row['pickID'] == $row['visitorID'])
                echo '<b class="winner-color">' . $row['visitorID'] . '</b>';
            else{
                echo '<b class="loser-color">' . $row['visitorID'] . '</b>';
            }
            echo'</span>';

        }
        echo '</ul>';
        echo '  <ul id="sortable">';
        for ($i = 1; $i <= count($matchups); $i++) {

            $row = $matchups[$i];
            if ($row['datePassed'] || strtotime('now') > strtotime(getCutoffDateTime($week)))
                continue;
            echo '<li  class="style_li ui-state-default ';
            if (!is_array($picks[$row['gameID']]) && !$row['datePassed'])
                echo 'ui-state-disabled disabled"';
            else
                echo 'enabled"';
            echo 'id="game' . $row['gameID'] . '" data-gameID="' . $row['gameID'] . '" data-teamID="\'' . $row['homeID'] . '\'" data-pts="' . $i . '"><span class="pts" data-value="' . ($i + 100) . '">' . $i . '</span><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>';
            echo '<span class="right-matchup">';
            if($row['pickID'] == $row['homeID'])
                echo '<b class="winner-color" id="w'.$row['homeID'].'">' . $row['homeID'] . '</b>';
            else{
                echo '<b class="loser-color" id="l'.$row['homeID'].'">' . $row['homeID'] . '</b>';
            }
            echo'</span>';
            echo '<img class="logo-right" src="images/logos/' . $row['homeID'] . '.svg" />';
            echo '<span class="vs"> vs </span>';
            echo '<img class="logo-right2" src="images/logos/' . $row['visitorID'] . '.svg" />';
            echo '<span class="right-matchup2">';
            if($row['pickID'] == $row['visitorID'])
                echo '<b class="winner-color" id="w'.$row['visitorID'].'">' . $row['visitorID'] . '</b>';
            else{
                echo '<b class="loser-color" id="l'.$row['visitorID'].'">' . $row['visitorID'] . '</b>';
            }
            echo'</span></li>';
        }
        echo '</ul><br>';
        //echo '<button class="btn btn-primary update-pts">Update Points</button>  ';
        echo '</div>';
    }
    ?>
    <script type="text/javascript" src="js/column_right.js"></script>
<?php
} else {
    ;
    ?>

    <!-- end countdown code -->

    <?php
    $weekStats = array();
    $playerTotals = array();
    $possibleScoreTotal = 0;
    calculateStats();

    $tmpWins = 0;
    $i = 1;
    if (is_array($playerTotals) && sizeof($playerTotals) > 0) {
        echo '		<div class="bg-success">' . "\n";
        echo '			<b>Current Leaders (pts):</b><br />' . "\n";
        //var_dump($playerTotals[7][points]);
        foreach ($playerTotals as $key => $row) {
            $points[$key] = $row[points];
            $name[$key] = $row[name];
        }
        array_multisort($points, SORT_DESC, $playerTotals);
        //d($playerTotals);
        foreach ($playerTotals as $playerID => $stats) {
            //if ($tmpWins < $stats[wins]) $tmpWins = $stats[wins]; //set initial number of wins
            //if next lowest # of wins is reached, increase counter

            //if wins is zero or counter is 3 or higher, break
            //if ($stats[wins] == 0 || $i > 3) break;
            echo '			' . $i . '. ' . $stats[name] . ' - ' . $stats[points] . (($stats[points] > 1) ? ' points' : ' point') . '<br />';
            $i++;
            //$tmpWins = $stats[wins]; //set last # wins
        }
        echo '		</div>' . "\n";
    }
    $tmpWins = 0;
    $i = 1;
    if (is_array($playerTotals) && sizeof($playerTotals) > 0) {
        //show top 3 winners
        echo '		<div class="bg-success">' . "\n";
        echo '			<b>Current Leaders (# wins):</b><br />' . "\n";
        arsort($playerTotals);
        foreach ($playerTotals as $playerID => $stats) {
            if ($tmpWins < $stats[wins]) $tmpWins = $stats[wins]; //set initial number of wins
            //if next lowest # of wins is reached, increase counter
            if ($stats[wins] < $tmpWins) $i++;
            //if wins is zero or counter is 3 or higher, break
            if ($stats[wins] == 0 || $i > 3) break;
            echo '			' . $i . '. ' . $stats[name] . ' - ' . $stats[wins] . (($stats[wins] > 1) ? ' wins' : ' win') . '<br />';
            $tmpWins = $stats[wins]; //set last # wins
        }
        echo '		</div>' . "\n";
    }
    $tmpScore = 0;
    $i = 1;
    if (is_array($playerTotals) && sizeof($playerTotals) > 0) {
        //show top 3 pick ratios
        echo '		<div class="bg-success">' . "\n";
        echo '			<b>Current Leaders (by pick %):</b><br />' . "\n";
        $playerTotals = sort2d($playerTotals, 'score', 'desc');

        foreach ($playerTotals as $playerID => $stats) {

            if ($tmpScore < $stats[score]) $tmpScore = $stats[score]; //set initial top score
            //if next lowest score is reached, increase counter
            if ($stats[score] < $tmpScore) $i++;
            //if score is zero or counter is 3 or higher, break
            if ($stats[score] == 0 || $i > 3) break;
            $pickRatio = $stats[score] . '/' . $possibleScoreTotal;
            $pickPercentage = number_format((($stats[score] / $possibleScoreTotal) * 100), 2) . '%';
            echo '			' . $i . '. ' . $stats[name] . ' - ' . $pickRatio . ' (' . $pickPercentage . ')<br />';
            $tmpScore = $stats[score]; //set last # wins
        }
        echo '		</div>' . "\n";
    }


    if (COMMENTS_SYSTEM !== 'disabled') {
        echo '		<p class="bg-success"><b>Taunt your friends!</b><br /><a href="' . $_SERVER['REQUEST_URI'] . '#comments">Post a comment</a> now!</p>' . "\n";
    }
}
//include('includes/comments.php');?>
<?php

function getHighPts($high_val){
    global $week, $user;

    $picks = getUserPicks($week, $user->userID);
    foreach ($picks as $pick) {
        if($pick['points'] == $high_val)
            $high_val--;
    }

    return $high_val;
}
function setLockedIds(){
    global $week, $user;
    $ids = array();
    $picks = getUserPicks($week, $user->userID);
    foreach ($picks as $pick) {

        if ($pick['datePassed']) {
            $ids[] = $pick['gameID'];
        }
    }

    return $ids;
}

?>

<script type="text/javascript">

</script>
