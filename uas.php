<?php
//uas -> update all scores if a score exists for both home and away teams
require('includes/config.php');
include_once 'includes/functions.php';

$week = (int)getCurrentWeek();
if(date('l') === "Tuesday"){
    $week--;
}
$url = "http://www.nfl.com/ajax/scorestrip?season=".SEASON_YEAR."&seasonType=REG&week=".$week;
//echo $url;
if ($xmlData = file_get_contents($url)) {
    $xml = simplexml_load_string($xmlData);
    $json = json_encode($xml);
    $games_json = json_decode($json, true);
}
$games_count = 0;
$count = 0;
foreach ($games_json['gms']['g'] as $games) {
    $games_count++;
    $week_from_xml = $games_json['gms']['@attributes']['w'];
    //var_dump($games);
    if($games['@attributes']['hs'] != "" AND $games['@attributes']['vs'] != ""){
        if($games['@attributes']['h'] === "LA")  $games['@attributes']['h'] = "LAR";
        if($games['@attributes']['v'] === "LA")  $games['@attributes']['v'] = "LAR";
        $sql = "update " . DB_PREFIX . "schedule ";
        $sql .= "set homeScore = " . $games['@attributes']['hs'] . ", visitorScore = " . $games['@attributes']['vs'] . ", overtime = " . (($games['@attributes']['q'] == 'FO') ? 1 : 0) . " ";
        $sql .= "where homeID = '" . $games['@attributes']['h'] ."' AND visitorID = '" .$games['@attributes']['v'] ."' AND weekNum = ".$week_from_xml." ;";
        $message = $games['@attributes']['h'] .  " " . $games['@attributes']['hs'] . " " . $games['@attributes']['v'] . " " . $games['@attributes']['vs'] ."</br>"; 
        echo $message;
        $count++;
        $mysqli->query($sql) or die('Error updating score: ' . $mysqli->error);
        //$mysqli_otherdb->query($sql) or die('Error updating score: ' . $mysqli->error);
    }
}
echo $count . " of " . $games_count;
