<?php
$week = (int)$_GET['week'];
if (empty($week)) {
    //get current week
    $week = (int)getCurrentWeek();
}
header("Location: entry.php?week=".$week);
exit();
$activeTab = 'entry';
require_once('includes/application_top.php');
require_once('includes/classes/team.php');
if (isset($_POST['ajax'])) {
    $sql = "select * from " . DB_PREFIX . "picks where userID = " . $user->userID . " and gameID = " . $_POST['gameID'];
    $q = $mysqli->query($sql);
    $row = $q->fetch_assoc();
    $tiePts = $row['tiePoints'];
    $q->free_result();
    $sql = "delete from " . DB_PREFIX . "picks where userID = " . $user->userID . " and gameID = " . $_POST['gameID'];
    $mysqli->query($sql) or die('Error deleting pick: ' . $mysqli->error);
    if(!is_null($tiePts))
        $sql = "insert into " . DB_PREFIX . "picks (userID, gameID, pickID, points, tiePoints) values (" . $user->userID . ", " . $_POST['gameID'] . ", " . $_POST['teamID'] . ", " . $_POST['points'] . ", " . $tiePts . ")";
    else
        $sql = "insert into " . DB_PREFIX . "picks (userID, gameID, pickID, points) values (" . $user->userID . ", " . $_POST['gameID'] . ", " . $_POST['teamID'] . ", " . $_POST['points'] . ")";
    $mysqli->query($sql) or die('Error ' . $mysqli->error);
    exit;

} else if ($_POST['ajax_pts']) {
    foreach ($_POST['items'] as $game) {
        $sql = "update " . DB_PREFIX . "picks set points = " . $game['points'] . " where  gameID = " . $game['gameID'] . " and userID = " . $user->userID . ";";
        $mysqli->query($sql) or die('Error inserting picks: ' . $mysqli->error);
    }
    exit;
    //var_dump($_POST['items']);
} else if ($_POST['ajax_tie']) {

    $sql = "update " . DB_PREFIX . "picks set tiePoints = " . $_POST['tiePoints'] . " where  gameID = " . $_POST['gameID'] . " and userID = " . $user->userID . ";";
    $mysqli->query($sql) or die('Error inserting picks: ' . $mysqli->error);


    //var_dump($_POST['items']);
} 
else if($_POST['syncpicks']){
    $week = (int)$_POST['week'];
    if (empty($week)) {
        //get current week
        $week = (int)getCurrentWeek();
    }
    //TODO check if week is expired
    $sql_2 = "SELECT * FROM " . DB_PREFIX . "picks as s INNER JOIN nflp_schedule as t ON s.gameID = t.gameID WHERE t.weekNum = ".$week." AND userID = ".$user->userID.";";
    $query = $mysqli->query($sql_2) or die($mysqli->error);
    $sql1 = "select userID from " . DB_PREFIX . "users WHERE email = '".$user->email."'";
    $q = $mysqli_otherdb->query($sql1) or die('Error getting userID from other DB: ' . $mysqli_otherdb->error);
    $row = $q->fetch_assoc();
    $userID = $row['userID'];
    $q->free_result();
    //echo $userID;
    while ($row = $query->fetch_assoc()) {
        $sql = "delete from " . DB_PREFIX . "picks where userID = " . $userID . " and gameID = " . $row['gameID'];
        $mysqli_otherdb->query($sql) or die('Error deleting pick: ' . $mysqli_otherdb->error);
        $tie = is_null($row['tiePoints']) ? "NULL" : $row['tiePoints']; 
        $sql1 = "insert into " . DB_PREFIX . "picks (userID, gameID, pickID, points, tiePoints) values (" . $userID . ", " . $row['gameID'] . ", '" . $row['pickID'] . "', " . $row['points'] . ", " . $tie. ")";
        $mysqli_otherdb->query($sql1) or die('Error inserting pick: ' . $mysqli_otherdb->error);
    }
    $query->free;
    exit;
}
else if($_POST['pickforme']){
        //var_dump($_POST);
        $week = (int)$_POST['week'];
        if (empty($week)) {
            //get current week
            $week = (int)getCurrentWeek();
        }
        //echo $week;
        $cutoffDateTime = getCutoffDateTime($week);
        $sql_2 = "select s.*, (DATE_ADD(NOW(), INTERVAL " . SERVER_TIMEZONE_OFFSET . " HOUR) > gameTimeEastern or DATE_ADD(NOW(), INTERVAL " . SERVER_TIMEZONE_OFFSET . " HOUR) > '" . $cutoffDateTime . "')  as expired ";
        $sql_2 .= "from " . DB_PREFIX . "schedule s ";
        $sql_2 .= "inner join " . DB_PREFIX . "teams ht on s.homeID = ht.teamID ";
        $sql_2 .= "inner join " . DB_PREFIX . "teams vt on s.visitorID = vt.teamID ";
        $sql_2 .= "where s.weekNum = " . $week . " ";
        $sql_2 .= "order by s.gameTimeEastern, s.gameID";
        $query = $mysqli->query($sql_2) or die($mysqli->error);
        
        if ($query->num_rows > 0) {
                //update summary table
            $sql = "delete from " . DB_PREFIX . "picksummary where weekNum = " . $week . " and userID = " . $user->userID . ";";
            $mysqli->query($sql) or die('Error updating picks summary: ' . $mysqli->error);
            $sql = "insert into " . DB_PREFIX . "picksummary (weekNum, userID, showPicks) values (" . $week . ", " . $user->userID . ", 1);";
            $mysqli->query($sql) or die('Error updating picks summary: ' . $mysqli->error);

            //echo $user->userID;
            $random_nums = UniqueRandomNumbersWithinRange(1,$query->num_rows,$query->num_rows);
            $i = 0;
            while ($row = $query->fetch_assoc()) {
                if(strtotime($row['gameTimeEastern']) > strtotime('now')){
                    if($i == $query->num_rows-1){
                        $tiepoints = rand(40, 70);
                    }
                    else $tiepoints = "NULL";
                    //var_dump($row);
                    $strings = array(
                        $row['homeID'],
                        $row['visitorID'],
                    );
                    $key = array_rand($strings);
                    //echo $random_nums[$i++] . " Pick " . $strings[$key];
                    $sql = "delete from " . DB_PREFIX . "picks where userID = " . $user->userID . " and gameID = " . $row['gameID'];
                    $mysqli->query($sql) or die('Error deleting picks: ' . $mysqli->error);
                    $sql = "insert into " . DB_PREFIX . "picks (userID, gameID, pickID, points, tiePoints) values (" . $user->userID . ", " . $row['gameID'] . ", '" . $strings[$key] . "', " . $random_nums[$i++] . ", ".$tiepoints.")";
                    //echo $sql . "/n";
                    $mysqli->query($sql) or die('Error inserting picks: ' . $mysqli->error);
                }
            }
            
        }
        exit;
}
else if ($_POST['action'] == 'Submit') {
    $week = $_POST['week'];
    $cutoffDateTime = getCutoffDateTime($week);

    //update summary table
    $sql = "delete from " . DB_PREFIX . "picksummary where weekNum = " . $_POST['week'] . " and userID = " . $user->userID . ";";
    $mysqli->query($sql) or die('Error updating picks summary: ' . $mysqli->error);
    $sql = "insert into " . DB_PREFIX . "picksummary (weekNum, userID, showPicks) values (" . $_POST['week'] . ", " . $user->userID . ", " . (int)$_POST['showPicks'] . ");";
    $mysqli->query($sql) or die('Error updating picks summary: ' . $mysqli->error);

    //loop through non-expire weeks and update picks
    $sql = "select * from " . DB_PREFIX . "schedule where weekNum = " . $_POST['week'] . " and (DATE_ADD(NOW(), INTERVAL " . SERVER_TIMEZONE_OFFSET . " HOUR) < gameTimeEastern and DATE_ADD(NOW(), INTERVAL " . SERVER_TIMEZONE_OFFSET . " HOUR) < '" . $cutoffDateTime . "');";
    $query = $mysqli->query($sql);
    if ($query->num_rows > 0) {
        while ($row = $query->fetch_assoc()) {
            $sql = "delete from " . DB_PREFIX . "picks where userID = " . $user->userID . " and gameID = " . $row['gameID'];
            $mysqli->query($sql) or die('Error deleting picks: ' . $mysqli->error);

            if (!empty($_POST['game' . $row['gameID']])) {
                $points = 0;
                $sql = "insert into " . DB_PREFIX . "picks (userID, gameID, pickID, points) values (" . $user->userID . ", " . $row['gameID'] . ", '" . $_POST['game' . $row['gameID']] . "', " . $points . ")";
                $mysqli->query($sql) or die('Error inserting picks: ' . $mysqli->error);
            } else {
                echo "test";
            }
        }
    }
    $query->free;
    header('Location: results.php?week=' . $_POST['week']);
    exit;
} else {
    $week = (int)$_GET['week'];
    if (empty($week)) {
        //get current week
        $week = (int)getCurrentWeek();
    }
    $cutoffDateTime = getCutoffDateTime($week);
    $firstGameTime = getFirstGameTime($week);
}

include('includes/header.php');
?>
<script type="text/javascript">
    function checkform() {
        //make sure all picks have a checked value
        var f = document.entryForm;
        var allChecked = true;
        var allR = document.getElementsByTagName('input');
        for (var i = 0; i < allR.length; i++) {
            if (allR[i].type == 'radio') {
                if (!radioIsChecked(allR[i].name)) {
                    allChecked = false;
                }
            }
        }
        if (!allChecked) {
            return confirm('One or more picks are missing for the current week.  Do you wish to submit anyway?');
        }
        return true;
    }
    function radioIsChecked(elmName) {
        var elements = document.getElementsByName(elmName);
        for (var i = 0; i < elements.length; i++) {
            if (elements[i].checked) {
                return true;
            }
        }
        return false;
    }
    function checkRadios() {
        $('input[type=radio]').each(function () {
            //alert($(this).attr('checked'));
            var targetLabel = $('label[for="' + $(this).attr('id') + '"]');
            //console.log($(this).attr('id') + ': ' + $(this).is(':checked'));
            if ($(this).is(':checked')) {
                //console.log(targetLabel);
                targetLabel.addClass('highlight');
            } else {
                targetLabel.removeClass('highlight');
            }
        });
    }
    $(function () {
        checkRadios();
        $('input[type=radio]').click(function () {
            checkRadios();
        });
        $('label').click(function () {
            checkRadios();
        });
    });
</script>
<?php
//display week nav
$sql = "select distinct weekNum from " . DB_PREFIX . "schedule order by weekNum;";
$query = $mysqli->query($sql);
$weekNav = '<div id="weekNav" class="row">';
$weekNav .= '	<div class="navbar3 col-xs-12"><b>Go to week:</b> ';
$i = 0;
if ($query->num_rows > 0) {
    while ($row = $query->fetch_assoc()) {
        if ($i > 0) $weekNav .= ' | ';
        if ($week !== (int)$row['weekNum']) {
            $weekNav .= '<a href="entry.php?week=' . $row['weekNum'] . '">' . $row['weekNum'] . '</a>';
        } else {
            $weekNav .= $row['weekNum'];
        }
        $i++;
    }
}
$query->free;
$weekNav .= '	</div>' . "\n";
$weekNav .= '</div>' . "\n";
echo $weekNav;
?>
<div class="row">
    <div class="col-md-4 col-xs-12 col-right">
        <?php
        //display schedule for week
        $sql_1 = "select s.*, CONCAT(vt.city, ' ', vt.team) as visitor, CONCAT(ht.city, ' ', ht.team) as home, (DATE_ADD(NOW(), INTERVAL " . SERVER_TIMEZONE_OFFSET . " HOUR) > gameTimeEastern or DATE_ADD(NOW(), INTERVAL " . SERVER_TIMEZONE_OFFSET . " HOUR) > '" . $cutoffDateTime . "')  as expired ";
        $sql_1 .= "from " . DB_PREFIX . "schedule s ";
        $sql_1 .= "inner join " . DB_PREFIX . "teams ht on s.homeID = ht.teamID ";
        $sql_1 .= "inner join " . DB_PREFIX . "teams vt on s.visitorID = vt.teamID ";
        $sql_1 .= "where s.weekNum = " . $week . " ";
        $sql_1 .= "order by s.gameTimeEastern, s.gameID";
        //if($_SERVER['request_uri'] == "index.php")
        $url = "http://xml.pinnaclesports.com/pinnaclefeed.aspx?sporttype=Football&sportsubtype=nfl";
        /*$xml=simplexml_load_string(file_get_contents($url)) or die("Error: Cannot create object");
        if ($xmlData = file_get_contents($url)) {
            $xml = simplexml_load_string($xmlData);
            $json = json_encode($xml);
            $xml_games = json_decode($json, true);
        }*/
        include('includes/column_right.php');
        ?>
    </div>
    <div id="content" class="col-md-8 col-xs-12">
        <h2>Week <?php echo '<span id="currweek">'.$week.'</span>'; ?> - Make Your Picks:</h2>

        <p>Please make your picks below for each game.</p>
        <?php 
        if (strtotime($cutoffDateTime) > strtotime('now')) {
            //echo '<button type="button" class="btn btn-primary" id="pickforme">Randomize picks</button>';
            //echo '<button type="button" class="btn btn-primary" id="syncpicks">Syncronize Picks</button>';
        }
        
        
        //get existing picks
        $picks = getUserPicks($week, $user->userID);
        //get show picks status
        $sql = "select * from " . DB_PREFIX . "picksummary where weekNum = " . $week . " and userID = " . $user->userID . ";";
        $query = $mysqli->query($sql);
        if ($query->num_rows > 0) {
            $row = $query->fetch_assoc();
            $showPicks = (int)$row['showPicks'];
        } else {
            $showPicks = 1;
        }
        $query->free;

        $t = 0;
        $query = $mysqli->query($sql_1) or die($mysqli->error);
        if ($query->num_rows > 0) {
            echo '<form name="entryForm" action="entry_form.php" method="post" onsubmit="">' . "\n";
            echo '<input type="hidden" name="week" value="' . $week . '" />' . "\n";
            echo '		<div class="row">' . "\n";
            echo '			<div class="col-xs-12">' . "\n";
            $i = 0;
            
            while ($row = $query->fetch_assoc()) {
                $all_games= $xml_games['events'];
                $display_home_team = $row['home'];
                $display_visitor_team = $row['visitor'];
                /*foreach($all_games['event'] as $game){
                    $visiting_team = $game['participants']['participant'][0]['participant_name'];
                    $home_team = $game['participants']['participant'][1]['participant_name'];
                    $visiting_spread = $game['periods']['period']['spread']['spread_visiting'];
                    $home_spread = $game['periods']['period']['spread']['spread_home']; 
                    if($row['home'] === $home_team && $row['visitor'] === $visiting_team){
                        if($home_spread < 0){
                            $display_home_team = $row['home'] . ' (' . $home_spread .')';
                            $display_visitor_team = $row['visitor'];
                        }
                        else if($visiting_spread < 0){
                            $display_visitor_team = $row['visitor'] . ' (' . $visiting_spread . ')';
                            $display_home_team = $row['home'];
                        }
                        else if(is_null($visiting_spread) || is_null($home_spread)){
                            $display_home_team = $row['home'];
                            $display_visitor_team = $row['visitor'];
                        }
                        else {
                            $display_home_team = $row['home'] . ' (Pick Em)';
                            $display_visitor_team = $row['visitor'];
                        }
                    }
					
                }*/
                $scoreEntered = false;
                $homeTeam = new team($row['homeID']);
                $visitorTeam = new team($row['visitorID']);
                $rowclass = (($i % 2 == 0) ? ' class="altrow"' : '');
                echo '				<div class="matchup">' . "\n";

                echo '					<div class="row bg-row1">' . "\n";
                if (strlen($row['homeScore']) > 0 && strlen($row['visitorScore']) > 0) {
                    //if score is entered, show score
                    $scoreEntered = true;
                    $homeScore = (int)$row['homeScore'];
                    $visitorScore = (int)$row['visitorScore'];
                    if ($homeScore > $visitorScore) {
                        $winnerID = $row['homeID'];
                    } else if ($visitorScore > $homeScore) {
                        $winnerID = $row['visitorID'];
                    };
                    //$winnerID will be null if tie, which is ok
                    echo '					<div class="col-xs-12 center"><b>Final: ' . $row['visitorScore'] . ' - ' . $row['homeScore'] . '</b></div>' . "\n";
                } else {
                    //else show time of game
                    if (date('l', strtotime($row['gameTimeEastern'])) == 'Thursday') $thursday_game = true;
                    echo '					<div class="col-xs-12 center"><span class="matchup-label">' . $display_visitor_team. ' at ' . $display_home_team . ' </span><span class="time-label">' . date('D n/j g:i a', strtotime($row['gameTimeEastern'])) . ' ET</span></div>' . "\n";
                }
                echo '					</div>' . "\n";
                echo '					<div class="row versus">' . "\n";
                echo '						<div class="col-xs-1"></div>' . "\n";
                echo '						<div class="col-xs-4">' . "\n";
                echo '<label for="' . $row['gameID'] . $visitorTeam->teamID . '" class="label-for-check"><div class="team-logo';
                if (!$row['expired']) {
                    echo ' submit-ajax';
                }

                if ($i + 1 == $num_rows && !isset($picks[$row['gameID']]['gameID']))  echo ' final-game';
                //if ($i + 1 == $num_rows) echo ' final-game';
                echo '" onclick="document.entryForm.game' . $row['gameID'] . '[0].checked=true;" data-gameID="' . $row['gameID'] . '" id="'.$visitorTeam->teamID.'" data-teamID="\'' . $visitorTeam->teamID . '\'" data-loserID="\'' . $homeTeam->teamID . '\'"><img src="images/logos/' . $visitorTeam->teamID . '.svg"  /></div></label>' . "\n";
                echo '						</div>' . "\n";
                echo '						<div class="col-xs-2" id='.$row['gameID'].'>'.$picks[$row['gameID']]['points'].'</div>' . "\n";
                //var_dump($picks);
                echo '						<div class="col-xs-4">' . "\n";
                echo '<label for="' . $row['gameID'] . $homeTeam->teamID . '" class="label-for-check"><div class="team-logo';
                if (!$row['expired']) {
                    echo ' submit-ajax';
                }

                if ($i + 1 == $num_rows && !isset($picks[$row['gameID']]['gameID'])) echo ' final-game';
                //if ($i + 1 == $num_rows) echo ' final-game';
                echo '" onclick="document.entryForm.game' . $row['gameID'] . '[1].checked=true;" data-gameID="' . $row['gameID'] . '" id="'.$homeTeam->teamID.'" data-teamID="\'' . $homeTeam->teamID . '\'" data-loserID="\'' . $visitorTeam->teamID . '\'"><img src="images/logos/' . $homeTeam->teamID . '.svg"  /></div></label>' . "\n";
                echo '						</div>' . "\n";
                echo '						<div class="col-xs-1"></div>' . "\n";
                echo '					</div>' . "\n";

                echo '					<div class="row bg-row2">' . "\n";
                echo '						<div class="col-xs-1"></div>' . "\n";
                echo '						<div class="col-xs-4 center">' . "\n";
                if (!$row['expired']) echo '							<input type="radio" class="check-with-label" name="game' . $row['gameID'] . '" value="' . $visitorTeam->teamID . '" id="' . $row['gameID'] . $visitorTeam->teamID . '"' . (($picks[$row['gameID']]['pickID'] == $visitorTeam->teamID) ? ' checked' : '') . ' />' . "\n";
                echo '						</div>' . "\n";

                echo '						<div class="col-xs-2"></div>' . "\n";
                echo '						<div class="col-xs-4 center">' . "\n";
                if (!$row['expired']) echo '							<input type="radio" class="check-with-label" name="game' . $row['gameID'] . '" value="' . $homeTeam->teamID . '" id="' . $row['gameID'] . $homeTeam->teamID . '"' . (($picks[$row['gameID']]['pickID'] == $homeTeam->teamID) ? ' checked' : '') . ' />' . "\n";
                echo '						</div>' . "\n";
                echo '						<div class="col-xs-1"></div>' . "\n";
                echo '</div>';

                echo '					<div class="row bg-row3">' . "\n";
                echo '						<div class="col-xs-4 center">' . "\n";
                //echo '							<div class="team">' . $visitorTeam->city . ' ' . $visitorTeam->team . '</div>'."\n";
                echo '							<div class="record" >Record: ' . getTeamRecord($visitorTeam->teamID) . '</div>' . "\n";
                echo '						</div>' . "\n";
                echo '						<div class="col-xs-4 center">' . "\n";
                echo '<div class="alert alert-success alert-success-update" id="' . $row['gameID'] . '-show" role="alert">Success!</div>';
                echo '<div class="alert alert-danger alert-success-update" id="' . $row['gameID'] . '-show-fail" role="alert">Failure!</div>';
                echo '						</div>' . "\n";
                echo '						<div class="col-xs-4 center">' . "\n";
                //echo '							<div class="team">' . $homeTeam->city . ' ' . $homeTeam->team . '</div>'."\n";
                echo '							<div class="record">Record: ' . getTeamRecord($homeTeam->teamID) . '</div>' . "\n";
                echo '						</div>' . "\n";
                echo '					</div>' . "\n";
                if (strtotime('now') >= strtotime($cutoffDateTime) || $row['expired']) {
                    //else show locked pick
                    echo '					<div class="row bg-row4">' . "\n";
                    $pickID = getPickID($row['gameID'], $user->userID);
                    if (!empty($pickID)) {
                        $statusImg = '';
                        $pickTeam = new team($pickID);
                        $pickLabel = $pickTeam->teamName;
                    } else {
                        $statusImg = '<img src="images/cross_16x16.png" width="16" height="16" alt="" class="status-image"/>';
                        $pickLabel = 'None Selected';
                    }
                    if ($scoreEntered) {
                        //set status of pick (correct, incorrect)
                        if ($pickID == $winnerID) {
                            $statusImg = '<img src="images/check_16x16.png" width="16" height="16" alt="" class="status-image"/>';
                        } else {
                            $statusImg = '<img src="images/cross_16x16.png" width="16" height="16" alt="" class="status-image"/>';
                        }
                    }

                    echo '						<div class="col-xs-12 center your-pick">' . $statusImg . ' <b>Your Pick: </b>';
                    echo $pickLabel;
                    if (date('l', strtotime($row['gameTimeEastern'])) == "Monday" && $row['expired'])
                        echo '<br /><b >Tie Breaker: </b><span id="tiePts">' . $picks[$row['gameID']]['tiePoints'] . '</span><br />';
                    echo '</div>' . "\n";
                    echo '					</div>' . "\n";
                }
                if (date('l', strtotime($row['gameTimeEastern'])) == "Monday") {
                    $visitor = $visitorTeam->city . " " . $visitorTeam->team;
                    $home_team = $homeTeam->city . " " . $homeTeam->team;
                    $id_monday = $row['gameID'];
                }
                echo '				</div>' . "\n";
                $i++;


            }

            echo '		</div>' . "\n";
            echo '		</div>' . "\n";
            if (!isset($picks[$id_monday]['pickID'])) {
                echo '<div class="hide-div">Tie Breaker Points</br>';
            } else if (isset($picks[$id_monday]['pickID'])) {
                echo '<div class="">Tie Breaker Points</br>';
            }
            ?>
<?php
            if (date('l', strtotime($picks[$id_monday]['gameTime'])) == 'Monday' && $picks[$id_monday]['datePassed']) {
                $tie = isset($picks[$id_monday]['tiePoints']) ? $picks[$id_monday]['tiePoints'] : "";
                echo '<p>' . $tie . '</p></br></br>';
            } else {
                $tie = isset($picks[$id_monday]['tiePoints']) ? $picks[$id_monday]['tiePoints'] : "";
                echo '<input type="textbox" name="tie" id="tie" value="' . $tie . '" data-tieID="' . $id_monday . '"></br></br>';

            }
            ?>

            <div class="alert alert-success show-class">
                <strong>Tie Breaker Updated!</strong>.
            </div>
            <?php
            echo '<p class="noprint" style="display:none"><input type="checkbox" name="showPicks" id="showPicks" value="1" ' . (($showPicks) ? ' checked="checked"' : '') . ' /> <label for="showPicks">Allow others to see my picks</label></p>' . "\n";
            echo '</div>';
        }//echo '<p class="noprint"><input type="submit" name="action" value="Submit" /></p>' . "\n";
        echo '</form>' . "\n";


        echo '	</div>' . "\n"; // end col
        echo '	</div>' . "\n"; // end entry-form row
        //echo '<div id="comments" class="row">';
        include('includes/comments.php');
        //echo '</div>';
        include('includes/footer.php');?>
        <div class="modal fade" id="myModal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">

                        <h4 class="modal-title" id="myModalLabel"><?php echo $visitor ." at ". $home_team?></h4>
                    </div>
                    <div class="modal-body">

                                <tr>
                                    <span style="margin-right: 15px;"><td>Tie Breaker:</td></span><td><input type="text" name="name" id="modal-pts" data-gameID="<?php echo $id_monday ?>" placeholder="Tie Breaker" value="<?php echo $tie?>"></td>
                                </tr>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary submit-modal" id="send">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="noPointsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">

                        <h4 class="modal-title" id="myModalLabel">No Points Entered!</h4>
                    </div>
                    <div class="modal-body">

                        <tr>
                            <span style="margin-right: 15px;"><td>You need to provide a tie breaker.</td></span>
                        </tr>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="js/entry_form.js"></script>
