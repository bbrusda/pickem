<?php
$activeTab = "standings";
require('includes/application_top.php');
@include_once('kint/Kint.class.php');
$weekStats = array();
$playerTotals = array();
$possibleScoreTotal = 0;
calculateStats();

include('includes/header.php');
?>
<h1>Standings</h1>
<div class="row">

	<div class="col-md-4 col-xs-12">
	  <b>By Points</b><br />
		<div class="table-responsive">
		<table class="table table-striped">
			<tr><th align="left">Player</th><th>Points</th></tr>
		<?php
				if (is_array($playerTotals) && sizeof($playerTotals) > 0) {
				foreach ($playerTotals as $key => $row) {
					$points[$key] = $row[points];
					$name[$key] = $row[name];
				}
				array_multisort($points, SORT_DESC, $playerTotals);
				foreach ($playerTotals as $playerID => $stats) {
					$rowclass = (($i % 2 == 0) ? ' class="altrow"' : '');
					echo '	<tr' . $rowclass . '><td>' . $stats[name] . '</td><td align="center">' . $stats[points] .'</td></tr>';
				}
				
		} else {
			echo '	<tr><td colspan="3">No weeks have been completed yet.</td></tr>' . "\n";
		}

		?>
		</table>
		</div>
	</div>

	<div class="col-md-4 col-xs-12">
		<b>By Pick Percentage</b><br />
		
		<div class="table-responsive">
			<table class="table table-striped">
				<tr><th align="left">Player</th><th align="left">Wins</th><th>Pick Ratio</th></tr>
			<?php
			if (isset($playerTotals)) {
				//arsort($playerTotals);
				$picked_correct = array();
				foreach ($playerTotals as $key => $row) {
					$picked_correct[$key] = $row[score];
					$name[$key] = $row[name];
				}
				array_multisort($picked_correct, SORT_DESC, $playerTotals);
				//d($playerTotals);
				$i = 0;
				foreach($playerTotals as $playerID => $stats) {
					$rowclass = (($i % 2 == 0) ? ' class="altrow"' : '');
					$pickRatio = $stats[score] . '/' . $possibleScoreTotal;
					$pickPercentage = number_format((($stats[score] / $possibleScoreTotal) * 100), 2) . '%';
					switch (USER_NAMES_DISPLAY) {
						case 1:
							echo '	<tr' . $rowclass . '><td class="tiny">' . $stats[name] . '</td><td class="tiny" align="center">' . $stats[wins] . '</td><td class="tiny" align="center">' . $pickRatio . ' (' . $pickPercentage . ')</td></tr>';
							break;
						case 2:
							echo '	<tr' . $rowclass . '><td class="tiny">' . $stats[userName] . '</td><td class="tiny" align="center">' . $stats[wins] . '</td><td class="tiny" align="center">' . $pickRatio . ' (' . $pickPercentage . ')</td></tr>';
							break;
						default: //3
							echo '	<tr' . $rowclass . '><td class="tiny"><abbr title="' . $stats[name] . '">' . $stats[userName] . '<abbr></td><td class="tiny" align="center">' . $stats[wins] . '</td><td class="tiny" align="center">' . $pickRatio . ' (' . $pickPercentage . ')</td></tr>';
							break;
					}
					$i++;
				}
			} else {
				echo '	<tr><td colspan="3">No weeks have been completed yet.</td></tr>' . "\n";
			}
			?>
			</table>
		</div>
	</div>
	<div class="col-md-4 col-xs-12">
		<b>By Wins</b><br />
		<div class="table-responsive">
			<table class="table table-striped">
				<tr><th align="left">Player</th><th align="left">Wins</th><th>Pick Ratio</th></tr>
			<?php
			if (isset($playerTotals)) {
				arsort($playerTotals);
				$i = 0;
				foreach($playerTotals as $playerID => $stats) {
					$rowclass = (($i % 2 == 0) ? ' class="altrow"' : '');
					$pickRatio = $stats[score] . '/' . $possibleScoreTotal;
					$pickPercentage = number_format((($stats[score] / $possibleScoreTotal) * 100), 2) . '%';
					switch (USER_NAMES_DISPLAY) {
						case 1:
							echo '	<tr' . $rowclass . '><td class="tiny">' . $stats[name] . '</td><td class="tiny" align="center">' . $stats[wins] . '</td><td class="tiny" align="center">' . $pickRatio . ' (' . $pickPercentage . ')</td></tr>';
							break;
						case 2:
							echo '	<tr' . $rowclass . '><td class="tiny">' . $stats[userName] . '</td><td class="tiny" align="center">' . $stats[wins] . '</td><td class="tiny" align="center">' . $pickRatio . ' (' . $pickPercentage . ')</td></tr>';
							break;
						default: //3
							echo '	<tr' . $rowclass . '><td class="tiny"><abbr title="' . $stats[name] . '">' . $stats[userName] . '</abbr></td><td class="tiny" align="center">' . $stats[wins] . '</td><td class="tiny" align="center">' . $pickRatio . ' (' . $pickPercentage . ')</td></tr>';
							break;
					}
					$i++;
				}
			} else {
				echo '	<tr><td colspan="3">No weeks have been completed yet.</td></tr>' . "\n";
			}
			?>
			</table>
		</div>
	</div>
	</div>
<div class="table-responsive">
<table class="table table-striped">
	<tr><th align="left">Week</th><th align="left">Winner(s)</th><th style="text-align: center;">Score</th></tr>
<?php
if (isset($weekStats)) {
    $i = 0;
    foreach($weekStats as $week => $stats) {
        $winners = '';
        if (is_array($stats[winners])) {
            foreach($stats[winners] as $winner => $winnerID) {
                $tmpUser = $login->get_user_by_id($winnerID);
                switch (USER_NAMES_DISPLAY) {
                    case 1:
                        $winners .= ((strlen($winners) > 0) ? ', ' : '') . trim($tmpUser->firstname . ' ' . $tmpUser->lastname);
                        break;
                    case 2:
                        $winners .= ((strlen($winners) > 0) ? ', ' : '') . $tmpUser->userName;
                        break;
                    default: //3
                        $winners .= ((strlen($winners) > 0) ? ', ' : '') . '<abbr title="' . trim($tmpUser->firstname . ' ' . $tmpUser->lastname) . '">' . $tmpUser->userName . '</abbr>';
                        break;
                }
            }
        }
        $rowclass = (($i % 2 == 0) ? ' class="altrow"' : '');
        echo '	<tr' . $rowclass . '><td>' . $week . '</td><td>' . $winners . '</td><td align="center">' . $stats[highestScore] .'</td></tr>';
        $i++;
    }
} else {
    echo '	<tr><td colspan="3">No weeks have been completed yet.</td></tr>' . "\n";
}

?>
</table>
</div>
<?php
include('includes/comments.php');

include('includes/footer.php');
?>