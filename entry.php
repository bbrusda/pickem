<?php
$activeTab = 'entry';
require_once('includes/application_top.php');
require_once('includes/classes/team.php');
@include_once('kint/Kint.class.php');
include('includes/header.php');
?>
<link rel="stylesheet" href="css/bootstrap-table-reorder-rows.css">
<?php
$week = (int)$_GET['week'];
if (empty($week)) {
    //get current week
    $week = (int)getCurrentWeek();
}
if($week == "1" || $week == 1){
    ?>
    <div class="form-group">
        <div class="alert alert-danger" style="font-size: 14px;">
            <strong>NOTICE***:</strong> With the cancellation of the Miami vs Tampa Bay game, please select Miami for 1 point. It will be a free point awarded this week
        </div>
    </div>
    <?php
}

//display week nav
$sql = "select distinct weekNum from " . DB_PREFIX . "schedule order by weekNum;";
$query = $mysqli->query($sql);
$weekNav = '<div id="weekNav" class="row">';
$weekNav .= '	<div class="navbar3 col-xs-12"><b>Go to week:</b> ';
$i = 0;
if ($query->num_rows > 0) {
    while ($row = $query->fetch_assoc()) {
        if ($i > 0) $weekNav .= ' | ';
        if ($week !== (int)$row['weekNum']) {
            $weekNav .= '<a href="entry.php?week=' . $row['weekNum'] . '">' . $row['weekNum'] . '</a>';
        } else {
            $weekNav .= $row['weekNum'];
        }
        $i++;
    }
}
$query->free;
$weekNav .= '	</div>' . "\n";
$weekNav .= '</div>' . "\n";
echo $weekNav;
//$tieBreakerGameTime = '2017-08-24 22:20:00';
$tieBreakerGameTime = getTieBreakerGameTime($week);
$tieBreakerGameDetails  = getTieBreakerGameDetails($week);
//var_dump(new DateTime($tieBreakerGameTime));
//var_dump(new DateTime(SERVER_TIMEZONE_OFFSET ." hour"));
if(new DateTime($tieBreakerGameTime) < new DateTime(SERVER_TIMEZONE_OFFSET ." hour")){
    $hide_tie = true;
}
else{
    $hide_tie = false;
}
//TODO show loader, update code to show correct tie breaker
?>
<script type="text/javascript" src="js/bootstrap-table.js"></script>
<script type="text/javascript" src="js/bootstrap-table-reorder-rows.js"></script>
<script type="text/javascript" src="js/jquery.tablednd.js"></script>
<script type="text/javascript" src="js/entry.js"></script>
<script type="text/javascript" src="js/bootbox.min.js"></script>

<table id="entry-table" data-reorderable-rows="true" data-use-row-attr-func="true"></table>
<div id="loader"></div>
<div class="form-horizontal">
<div class="form-group">  
  <div class="col-md-4 col-md-offset-4">
  <input id="tie_breaker" name="tie_breaker" type="text" placeholder="Tie Breaker" class="form-control input-md" <?php echo $hide_tie ? "disabled" : ""; ?>>
  <span class="help-block">Combined score <img class="logo-desktop logo" title="<?php echo $tieBreakerGameDetails["visitorID"] ?>" src="images/logos/<?php echo $tieBreakerGameDetails["visitorID"] ?>.svg">@<img class="logo-desktop logo" title="<?php echo $tieBreakerGameDetails["homeID"] ?>" src="images/logos/<?php echo $tieBreakerGameDetails["homeID"] ?>.svg"></span>  
  </div>
</div>
</div>
<img class="col-md-offset-6" id="small-loader" src='images/loading.gif' alt='loading'/>
<button class="btn btn-primary col-md-6 col-md-offset-3 col-sm-12 col-xs-12" id="save">Save</button>
<table id="expired-table" data-row-style="expiredRow"></table></br>



