<?php
require('includes/config.php');
require('includes/functions.php');
@include_once('kint/Kint.class.php');
require VENDOR_PATH . 'autoload.php';

use Mailgun\Mailgun;

//Your credentials
$mg = new Mailgun("key-6ebd26904baca68ae66aeb6454df552d");
$domain = "nflconfidencepicks.com";
$week = (int)getCurrentWeek();

//$week = 6;
$totalGames = getGameTotal($week);


//select only users missing picks for the current week
$sql = "select u.firstname, u.email, u.userName, ";
$sql .= "(select count(p.pickID) from nflp_picks p inner join nflp_schedule s on p.gameID = s.gameID where userID = u.userID and s.weekNum = " . $week . ") as userPicks ";
$sql .= "from " . DB_PREFIX . "users u ";
$sql .= "where u.`status` = 1 and u.userName <> 'admin' ";
$sql .= "group by u.firstname, u.email ";
$sql .= "having userPicks < " . $totalGames;
$query = $mysqli->query($sql);
if ($query->num_rows > 0) {
    while ($row = $query->fetch_assoc()) {
        $full_html = file_get_contents(__DIR__ ."/emails/picksreminder.html");
        $full_html = str_replace("%week%", $week, $full_html);
        $full_html = str_replace("%site_url%", SITE_URL . 'login.php', $full_html);
//        if ($row["email"] == "ryanmontgomery6@gmail.com" || $row["email"] == "pmortell1@yahoo.com" || $row["email"] == "bdbtzc@gmail.com") {
//            $full_html = str_replace("%first_name%", "Faggot", $full_html);
//        } else {
//            $full_html = str_replace("%first_name%", $row["firstname"], $full_html);
//        }
        $full_html = str_replace("%first_name%", $row["firstname"], $full_html);
        //if($row["email"] == "bdbtzc@gmail.com") {
            $result = $mg->sendMessage($domain, array(
                    'from' => 'noreply@nflconfidencepicks.com',
                    'to' => $row['email'],
                    'subject' => "Weekly Picks Reminder",
                    'html' => $full_html
                )
            );
        //}
//        echo $full_html;


        //echo $subject . '<br />';

    }

}