<?php
/**
 * Created by PhpStorm.
 * User: bbrusda
 * Date: 06/16/2017
 * Time: 3:19 PM
 */
require_once('includes/application_top.php');
require_once 'includes/Medoo.php';
use Medoo\Medoo;

$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => DB_DATABASE,
    'server' => DB_HOSTNAME,
    'username' => DB_USERNAME,
    'password' => DB_PASSWORD,
    'charset' => 'utf8'
]);
$games = array();
$games = $_POST["data"];
//$week = $_POST['week'];
$week = (int)$_POST['week'];
if (empty($week)) {
    //get current week
    $week = (int)getCurrentWeek();
}
$tieBreakerGameTime = getTieBreakerGameTime($week);
if(new DateTime($tieBreakerGameTime) < new DateTime(SERVER_TIMEZONE_OFFSET ." hour")){
    //game has started
    $change_tie = false;
}
else{
    $change_tie = true;
}
$tie_breaker = $_POST["tie_breaker"];
$success = true;
$userid = $user->userID;
foreach($games as $game){
    if($game["selected_home"] == "0" && $game["selected_visitor"] == "0"){
        //no one picked
        continue;
    }
    else {
        $gameid = $game["gameID"];
        $points = $game["points"];
        $pickid = "";
        if ($game["selected_home"] == "1") {
            $pickid = $game["homeID"];
        } elseif ($game["selected_visitor"] == "1"){
            $pickid = $game["visitorID"];
        }
            if ($database->has("nflp_picks", [
                "AND" => [
                    "userID" => $userid,
                    "gameID" => $gameid
                ]
            ])) 
            {
                //update
                $update_record = $database->update("nflp_picks", [
                    "points" => $points,
                    "pickID" =>$pickid
                ], [
                    "userID" => $userid,
                    "gameID" => $gameid
                ]);
                
            } else {
                //insert
                $insert_record = $database->insert("nflp_picks", [
                    "userID" => $userid,
                    "gameID" => $gameid,
                    "pickID" => $pickid,
                    "points" => $points
                ]);
                
            }
            if(!checkError($database->error()[0])){
                $success = false;
            }
    }
}
if($success && $change_tie){
            if ($database->has("nflp_picksummary", [
                "AND" => [
                    "userID" => $userid,
                    "weekNum" => $week
                ]
            ])) 
            {
                //update
                $update_record = $database->update("nflp_picksummary", [
                    "tieBreakerPoints" => $tie_breaker,
                ], [
                    "userID" => $userid,
                    "weekNum" => $week
                ]);
            } else {
                //insert
                $insert_record = $database->insert("nflp_picksummary", [
                    "userID" => $userid,
                    "weekNum" => $week,
                    "tieBreakerPoints" => $tie_breaker,
                ]);
                
            }
            if(!checkError($database->error()[0])){
                $success = false;
            }
}
if(!$success){
    echo 'fail';
}
else{
    echo 'success';
}
function checkError($code){
    if($code == "00000"){
        return true;
    }
    else{
        return false;
    }
}